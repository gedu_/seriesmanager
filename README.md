# Series manager

Tracks when series episodes will be live and then downloads it from TPB. Can utilise [VCS](https://gitlab.com/gedu_/vcs) to convert videos to format which can be shown using browser video tag. Provides UI to view directory structure and view videos through browser (if video format is supported), delete and track progress of torrent files, add and delete series to track.

## Features

+ Tracking series (using TVDB database)

+ Finding episodes torrents (using TPB)

+ Downloading torrents (using Transmission)

+ Converting videos to format viewable from browser (using VCS)

+ UI to view directory structure and videos, manage series and torrents

## Requirements

These are required to run minimal setup:

+ Mysql/MariaDB RDBMS

These are required to run full setup:

+ TVDB account for integrating with tvdb

+ Transmission-remote to track downloads

+ Transmission-daemon to do the actual downloading

+ VCS to convert videos

+ FFProbe (Part of FFMpeg) to gather stats about video files (length)

These are optional:

+ Curl

## Set up

### Transmission-daemon

1. Install transmission-daemon
2. Stop transmission-daemon service
3. Edit settings.json
    1. `download-dir` to directory in which all videos supposed to be
    2. `rpc-authentication-required` => true
    3. `rpc-bind-address` => your local IP (for example 192.168.1.2)
    4. `rpc-enabled` => true
    5. `rpc-password` => password for transmission-daemon (it will be encoded once service starts)
    6. `rpc-username` => username for transmission-daemon
    7. `rpc-whitelist` => list of ip addresses that can access rpc web browser. If transmission-remote is on the same machine as this service you need to add 127.0.0.1. IPS are delimited by a comma (for example "127.0.0.1,192.168.1.3")
    8. `rpc-whitelist-enabled` => true
    9. `umask` => might need to set this value to 18
4. Start transmission-daemon
5. Test by going to the site **(don't forget to change the IP)** http://192.168.1.2:9091/transmission and enter credentials from `rpc-username` and `rpc-password`

### HTTP Server for UI Component and DataRoot

Setup two virtual hosts in your favourite HTTP server:

1. Point to ui/src folder (All JS files are bundled, no npm install is required)
2. Point to DataRoot folder (defined in backend config.json)

### Backend as a service

#### Linux

1. Run `npm install` if not done before
2. Run `npm run-script build`
3. Change User, WorkingDirectory, ExecStart variables in seriesmanager.service
4. Copy seriesmanager.service to appropriate folder in your distribution, for example: `/etc/systemd/system/multi-user.target.wants/`
5. `systemd enable seriesmanager`
6. `systemd start seriesmanager`

#### Windows

1. Run `npm install` if not done before
2. Run `npm run-script build`
3. Download nssm from [here](https://nssm.cc/)
4. Run `.\nssm.exe install seriesmanager`
5. Set Application path to run_static.bat or run_dynamic.bat (dynamic uses nodemon instead of node)
6. Set Application startup directory to parent directory of service (same folder where .bat files are located)
7. Click `Install Service`
8. Run `.\nssm.exe start seriesmanager`

## Configuration

### Backend

Update config.json file in service folder

#### Database Configuration

```json
"Database": {
    "Host": "localhost",
    "User": "user",
    "Pass": "pass",
    "Db": "db"
}
```

#### Transmission-remote configuration

Login is username and password delimited by ':'

```json
"Transmission": {
    "Host": "127.0.0.1:9091",
    "Login": "user:pass",
    "Path": "/usr/bin/transmission-remote",
    "Enabled": false
},
```

#### TVDB API configuration

Userkey is Unique ID in TVDB site

```json
"Tvdb": {
    "User": "user",
    "Userkey": "userkey",
    "Apikey": "apikey",
    "Enabled": false
},
```

#### FFProbe configuration

```json
"FFProbe": {
    "Path": "/usr/bin/ffprobe",
    "Enabled": false
},
```

#### Curl configuration

```json
"Curl": {
    "Path": "/usr/bin/curl",
    "Enabled": false
},
```

#### VCS configuration

```json
"VCS": {
    "Endpoint": "127.0.0.1",
    "Port": 3005,
    "Enabled": true
},
```

#### Other configuration

ExcludeTags: strings in TPB torrent title that should discard the torrent from download. For example: "x265" -> don't download videos which are encoded in x265 (HEVC)

DataRoot: root folder for videos and where new videos should be downloaded

InputVideoTypes: video types that are scanned for conversion if VCS is used

OutputVideoTypes: video types that will be shown in the UI

UpdateCheckIntervalMinutes: interval how often system should check wheter an episode can be downloaded

Port: output port of this API

Cors: URLs which are allowed to call this API (should match UI component URL)

```json
"ExcludeTags": ["x265"],
"DataRoot": "/media/",
"InputVideoTypes": ["avi", "m4v", "asf", "wmv", "mpeg", "ogv", "webm", "mkv" ],
"OutputVideoTypes": ["mp4"],
"UpdateCheckIntervalMinutes": 30,
"Port": 3000,
"Cors": ["http://127.0.0.1:9000"],
"Logfile": "logs\\sm.log"
```

### Frontend

Update constants.js file in ui/src folder.

HOST is the address to Backend API

VIDEOS_HOST is the address to videos DataRoot hosted on some HTTP Server

```js
HOST: 'http://127.0.0.1:3000/',
VIDEOS_HOST: 'http://127.0.0.1/videos'
```