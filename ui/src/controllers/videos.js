angular.module('vids.ui').controller('videos', ['$scope', '$uibModal', '$http', 'URLS', '$window', 'copyService', 'toaster', 'ALERT_TYPE', '$interval', '$sce', controller]);


function controller($scope, $uibModal, $http, URLS, $window, copyService, toaster, ALERT_TYPE, $interval, $sce) {
    $scope.loading = false;
    $scope.showVideo = false;
    $scope.paths = [];
    $scope.items = [];

    var showAll = false;
    var base = [];
    var basePath = {
        Name: 'Videos',
        Id: 0
    };

    $scope.isViewedPredicate = function (item) {
        if (showAll) {
            return true;
        }

        if (item.Type === 1) {
            return item.Children.length > 0 ? !item.IsViewed : false;
        }

        return !item.IsViewed;
    };

    $scope.hasItems = function (item) {
        if (item.Type === 1) {
            if(item.IsViewed) {
                return false;
            }
            
            return item.Children.filter(function (it) {
                return $scope.isViewedPredicate(it);
            }).length > 0;
        }

        return true;
    };

    $scope.base = {
        value: 'new',
        values: [
            {
                text: 'New',
                value: 'new'
            },
            {
                text: 'All',
                value: 'all'
            }
        ],
        callback: function () {
            showAll = $scope.base.value === 'all';
        }
    };

    $scope.callbacks = {
        open: open,
        view: view,
        url: url,
        edit: edit,
        seen: seen,
        viewPredicate: $scope.isViewedPredicate
    };    

    $scope.transformViewUrl = function (url) {
        var fixedPath = url.replace(/\\/g, "/");
        return URLS.VIDEOS_HOST + fixedPath;
    }

    ///////////////////////////////////////////////////

    function init () {
        var url = URLS.HOST + URLS.Videos.All;
        $scope.loading = true;

        $http.get(url).then(
            function (response) {
                base = angular.copy(response.data);
                sortItems(base);

                $scope.items = base;
                $scope.paths = [basePath];

                $scope.loading = false;
            },
            function (err) {
                console.warn(err);
                toaster.showToaster('Failed to load data from the server', ALERT_TYPE.Danger);
                $scope.loading = false;
            }
        );
    }

    init();
    
    ///////////////////////////////////////////////////

    function recursiveOpen (obj, paths, items, id) {
        for (var name in obj) {
            if (obj[name].Id === id) {
                paths.push({
                    Name: obj[name].Name,
                    Id: obj[name].Id
                });

                if (obj[name].Children) {
                    obj[name].Children.forEach(function (it) {
                        items.push(it);
                    });
                }                

                return true;
            } else if (obj[name].IsDir) {
                if (recursiveOpen(obj[name].Children, paths, items, id)) {
                    paths.push({
                        Name: obj[name].Name,
                        Id: obj[name].Id
                    });

                    return true;
                }
            }
        }
    }

    ///////////////////////////////

    function sortItems (items) {
        items.sort(function (a, b) {
            if (a.Type > b.Type) {
                return 1;
            } else if (a.Type < b.Type) {
                return -1;
            } else {
                return a.Name.localeCompare(b.Name);
            }
        });
    }

    ///////////////////////////////

    function edit (item) {
        var modal = $uibModal.open({
            controller: 'videosEdit',
            size: 'md',
            templateUrl: 'templates/modals/videos.edit.html',
            resolve: {
                item: item
            }
        });

        modal.result.then(
            function (res) {
                sortItems($scope.items);
            },
            function () {
                // Workaround to avoid console errors about dismissal
            }
        );
    };

    ///////////////////////////////

    function seen (item) {
        var url = URLS.HOST + URLS.Videos.Edit.replace(':id', item.Id);

        item.IsViewed = true;

        $http.patch(url, item).then(
            function (response) {
                toaster.showToaster('Item ' + item.Name + ' updated.', ALERT_TYPE.Success);
                sortItems($scope.items);     
            },
            function (err) {
                console.warn(err);
            }
        );
    };

    ///////////////////////////////

    function open (item) {
        $scope.showVideo = false;

        if (item.Id === 0) {
            $scope.items = base;
            $scope.paths = [basePath];           
        } else {
            var paths = [];
            var items = [];

            recursiveOpen(base, paths, items, item.Id);

            paths.push(basePath);
            paths.reverse();

            sortItems(items);

            $scope.items = items;
            $scope.paths = paths;
        }
    };

    ///////////////////////////////

    function view (item, index) {
        item.Children.sort(function (a, b) {
            return a.Name.localeCompare(b.Name);
        });

        var paths = [];
        var items = [item];

        recursiveOpen(base, paths, items, item.Id);

        paths.push(basePath);
        paths.reverse();

        $scope.showVideo = true;
        $scope.items = items;
        $scope.video = items[0].Languages[index];
        $scope.paths = paths;
        $scope.currentVideoSeen = false;
        $scope.continueViewing = {
            show: false,
            time: 0
        };

        var checkInterval = $interval(function () {
            var elem = document.getElementById('video');

            if (elem) {
                $interval.cancel(checkInterval);

                var savedSession = window.localStorage.getItem($scope.items[0].Id);

                // Show continue button for the first 10 seconds if used watched it previously (assuming that video is not seen before)
                if (savedSession !== null && Math.ceil(elem.currentTime) < 10) {
                    $scope.continueViewing = {
                        show: true,
                        time: JSON.parse(savedSession).time
                    };
                }

                elem.ontimeupdate = function () {    
                    var onePercent = 100 / elem.duration;
                    var currentPercentage = onePercent * elem.currentTime;
                    var threshold = 90;
                               
                    if (Math.ceil(currentPercentage) > threshold && $scope.showVideo && !$scope.currentVideoSeen) {
                        seen($scope.items[0]);
                        $scope.currentVideoSeen = true;
                    }

                    // update how much is viewed
                    if (Math.ceil(elem.currentTime) > 10 && !elem.paused) {
                        $scope.continueViewing.show = false;

                        var obj = {
                            time: elem.currentTime
                        };

                        localStorage.setItem($scope.items[0].Id, JSON.stringify(obj));

                        $scope.$apply();
                    }
                }
            }
        }, 200);        
    };

    ///////////////////////////////

    function url (item) {
        var fixedPath = item.RelativePath.replace(/\\/g, "/");

        copyService.copy(URLS.VIDEOS_HOST + fixedPath);
        toaster.showToaster('Url copied to clipboard.', ALERT_TYPE.Success);
    }

    ///////////////////////////////

    $scope.continue = function () {
        var elem = document.getElementById('video');
        elem.currentTime = $scope.continueViewing.time;
        $scope.continueViewing.show = false;
        elem.play();
    }
    
    ///////////////////////////////
}
