angular.module('vids.ui').controller('series', ['$scope', '$uibModal', '$http', 'URLS', function($scope, $uibModal, $http, URLS) {
    $scope.data = [];
    $scope.episodes = [];
    $scope.activeTab = 0;

    function getData () {
        var url = URLS.HOST + URLS.Series.List;
        var episodesUrl = URLS.HOST + URLS.Series.Episodes;

        $http.get(url).then(
            function (response) {
                $scope.data = response.data;
            },
            function (err) {
                console.warn(err);
            }
        );

        $http.get(episodesUrl).then(
            function (response) {
                var tmp = angular.copy(response.data);

                // Sort by date, closest to today on top
                tmp.sort(function (a, b) {
                    return a.AiredTime > b.AiredTime ? 1 : (a.AiredTime < b.AiredTime ? -1 : 0);
                });

                $scope.episodes = tmp;
            },
            function (err) {
                console.warn(err);
            }
        );
    }

    function init () {
        getData();
    }

    $scope.add = function () {
        var modal = $uibModal.open({
            controller: 'seriesAdd',
            size: 'lg',
            templateUrl: 'templates/modals/series.add.html',
        });

        modal.result.then(
            function (res) {
                if (res) {
                    getData();
                }
            },
            function () {
                // Workaround to avoid console errors about dismissal
            }
        );
    }

    $scope.edit = function (item) {
        var modal = $uibModal.open({
            controller: 'seriesEdit',
            size: 'lg',
            templateUrl: 'templates/modals/series.edit.html',
            resolve: { data: { item: item } }
        });

        modal.result.then(
            function (res) {
                if (res) {
                    getData();
                }
            },
            function () {
                // Workaround to avoid console errors about dismissal
            }
        );
    }

    $scope.delete = function (item) {
        var modal = $uibModal.open({
            controller: 'seriesDelete',
            size: 'md',
            templateUrl: 'templates/modals/series.delete.html',
            resolve: { data: { item: item } }
        });

        modal.result.then(
            function (res) {
                if (res) {
                    getData();
                }
            },
            function () {
                // Workaround to avoid console errors about dismissal
            }
        );
    }

    init();
}]);