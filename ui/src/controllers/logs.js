angular.module('vids.ui').controller('logs', ['$scope', '$http', 'URLS', function ($scope, $http, URLS) {
    $scope.isLoading = false;
    $scope.logs = [];

    var today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);

    $scope.refresh = function (day) {
        var url = URLS.HOST + URLS.Logs.AnyDay.replace(':timestamp', day);
        $scope.isLoading = true;

        $http.get(url).then(function (response) {
            $scope.logs = angular.copy(response.data);
            // Flip array to show newest logs at the top
            $scope.logs.reverse();
            $scope.isLoading = false;
        }, function (err) {
            console.warn(err);
        });
    };

    function init () {
        $scope.refresh(today.getTime());
    }

    init();
}]);