angular.module('vids.ui').controller('seriesDelete', ['$scope', 'toaster', 'ALERT_TYPE', '$http', 'URLS', 'data',
function ($scope, toaster, ALERT_TYPE, $http, URLS, data) {
    $scope.seriesName = data.item.Name;

    $scope.finish = function () {
        var url = URLS.HOST + URLS.Series.Delete.replace(':id', data.item.Id);
        
        $http.delete(url).then(
            function (response) {
                $scope.isLoading = false;
                toaster.showToaster('Series ' + $scope.seriesName + ' deleted.', ALERT_TYPE.Success);
                $scope.$close(true);
            },
            function (err) {
                console.warn(err);
            }
        );
    }
}]);