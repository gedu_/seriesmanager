angular.module('vids.ui').controller('seriesEdit', ['$scope', 'toaster', 'ALERT_TYPE', '$http', 'URLS', 'data',
function ($scope, toaster, ALERT_TYPE, $http, URLS, data) {
    
    $scope.seriesName = data.item.Name;
    $scope.seasons = [];
    $scope.episodes = [];
    $scope.endEpisodes = [];
    $scope.isLoading = true;

    var id = data.item.Id;
    var url = URLS.HOST + URLS.Series.Stats.replace(':id', data.item.TvdbId);

    function getEpisodesBySeason (arr, value) {
        var episodes = arr.filter(function (item) {
            return item.number === value;
        })[0].episodes;

        for (var i = 0; i < episodes.length; i++) {
            episodes[i].episodeName = episodes[i].episode;
        }

        if (episodes[0].episodeName !== 0) {
            episodes.unshift({
                episodeName: 'No episodes seen this season',
                episode: 0,
                id: 0
            });
        }

        return episodes;
    }

    $scope.$watch('model.season', function (newValue, oldValue)  {
        if (newValue === oldValue) {
            return;
        }

        $scope.episodes = getEpisodesBySeason($scope.seasons, newValue);
    });

    $scope.$watch('model.endSeason', function (newValue, oldValue)  {
        if (newValue === oldValue) {
            return;
        }

        $scope.endEpisodes = getEpisodesBySeason($scope.seasons, newValue);
    });

    $http.get(url).then(
        function (response) {
            $scope.seasons = response.data;

            $scope.episodes = getEpisodesBySeason($scope.seasons, data.item.Season);
            $scope.endEpisodes = getEpisodesBySeason($scope.seasons, data.item.EndSeason);

            $scope.model = {
                season: data.item.Season,
                episode: $scope.episodes.filter(function (it) {
                    return it.episode === data.item.Episode
                })[0],
                endSeason: data.item.EndSeason,
                endEpisode: $scope.episodes.filter(function (it) {
                    return it.episode === data.item.EndEpisode
                })[0],
                limit: data.item.Limit.data[0] == 1
            };

            $scope.isLoading = false;
        }
    );

    $scope.finish = function () {
        $scope.step = 2;
        $scope.isLoading = true;

        var url = URLS.HOST + URLS.Series.Edit.replace(':id', id);

        var data = {
            season: $scope.model.season,
            episode: $scope.model.episode.episode,
            limit: $scope.model.limit,
            endSeason: $scope.model.endSeason,
            endEpisode: $scope.model.endEpisode ? $scope.model.endEpisode.episode : null,
        };

        $http.patch(url, data).then(
            function (response) {
                $scope.isLoading = false;
                toaster.showToaster('Series ' + $scope.seriesName + ' edited.', ALERT_TYPE.Success);
                $scope.$close(true);
            },
            function (err) {
                console.warn(err);
            }
        );
    }
}]);