angular.module('vids.ui').controller('videosDelete', ['$scope', 'item', 'toaster', 'ALERT_TYPE', '$http', 'URLS', controller]);

// TODO: remove this
function controller($scope, item, toaster, ALERT_TYPE, $http, URLS) {  

    $scope.delete = function () {
        var url = URLS.HOST + URLS.Videos.Delete.replace(':id', item.Id);
        //TODO: request to endpoint here !
        $http.delete(url).then(
            function (response) {
                console.warn(response);
            },
            function (err) {
                console.warn(err);
            }
        );

        toaster.showToaster('Item ' + item.Name + ' deleted.', ALERT_TYPE.Success);
        $scope.$close(true);        
    }
}