angular.module('vids.ui').controller('downloads', ['$scope', '$http', 'URLS', '$timeout', function ($scope, $http, URLS, $timeout) {
    $scope.isLoadingActive = false;
    $scope.isLoadingPast = false;
    $scope.isLoadingQueue = false;
    $scope.isLoadingConversions = false;
    $scope.activeTab = 1;

    $scope.activeDownloads = [];

    $scope.refreshActiveDownloads = function () {
        var url = URLS.HOST + URLS.Downloads.Active;
        $scope.isLoadingActive = true;

        $http.get(url).then(function (response) {
            $scope.activeDownloads = angular.copy(response.data);
            $scope.isLoadingActive = false;
        }, function (err) {
            console.warn(err);
        });
    };

    $scope.refreshPastDownloads = function () {
        var url = URLS.HOST + URLS.Downloads.Past;
        $scope.isLoadingPast = true;

        $http.get(url).then(function (response) {
            $scope.pastDownloads = angular.copy(response.data);
            $scope.isLoadingPast = false;
        }, function (err) {
            console.warn(err);
        });
    }

    $scope.refreshQueue = function () {
        var url = URLS.HOST + URLS.Downloads.Queue.List;
        $scope.isLoadingQueue = true;

        $http.get(url).then(function (response) {
            $scope.queue = angular.copy(response.data);
            $scope.isLoadingQueue = false;
        }, function (err) {
            console.warn(err);
        });
    }

    $scope.refreshConversions = function () {
        var url = URLS.HOST + URLS.Conversions.List;
        $scope.isLoadingConversions = true;

        $http.get(url).then(function (response) {
            $scope.conversions = angular.copy(response.data);
            $scope.isLoadingConversions = false;
        }, function (err) {
            console.warn(err);
        });
    }

    $scope.deleteActiveDownload = function (item) {
        var url = URLS.HOST + URLS.Downloads.Delete.replace(':id', item.Id);
        $scope.isLoadingActive = true;

        $http.delete(url).then(function (response) {
            $timeout(function () {
                $scope.refreshActiveDownloads();
            }, 300);       
        }, function (err) {
            console.warn(err);
        });
    };

    $scope.deletePastDownload = function (item) {
        var url = URLS.HOST + URLS.Downloads.Delete.replace(':id', item.Id);
        $scope.isLoadingPast = true;

        $http.delete(url).then(function (response) {
            $timeout(function () {
                $scope.refreshPastDownloads();
            }, 300);            
        }, function (err) {
            console.warn(err);
        });
    };

    $scope.deleteQueue = function (item) {
        var url = URLS.HOST + URLS.Downloads.Queue.Delete.replace(':id', item.EpisodeId);
        $scope.isLoadingQueue = true;

        $http.delete(url).then(function (response) {
            $timeout(function () {
                $scope.refreshQueue();
            }, 300);            
        }, function (err) {
            console.warn(err);
        });
    };

    $scope.deleteConversion = function (item) {
        var url = URLS.HOST + URLS.Conversions.Delete.replace(':id', item.Id);
        $scope.isLoadingConversions = true;

        $http.delete(url).then(function (response) {
            $timeout(function () {
                $scope.refreshConversions();
            }, 300);            
        }, function (err) {
            console.warn(err);
        });
    };

    function init () {
        $scope.refreshActiveDownloads();
        $scope.refreshPastDownloads();
        $scope.refreshQueue();
        $scope.refreshConversions();
    }

    init();
}]);