angular.module('vids.ui').controller('seriesAdd', ['$scope', 'utils', 'toaster', '$timeout', 'ALERT_TYPE', '$http', 'URLS', 
function ($scope, utils, toaster, $timeout, ALERT_TYPE, $http, URLS) {

    $scope.seriesName = '';
    $scope.progress = 0;
    $scope.model = {};
    $scope.model.series = null;
    $scope.model.season = null;
    $scope.model.episode = null;
    $scope.model.endSeason = null;
    $scope.model.endEpisode = null;
    $scope.model.limit = false;
    $scope.series = [];
    $scope.seasons = [];
    $scope.episodes = [];
    $scope.step = 0;
    $scope.searched = false;

    $scope.search = function () {
        if (!$scope.isLoading) {
            $scope.model.seriesSelected = null;
            $scope.progress = 25;
            $scope.isLoading = true;
            $scope.searched = false;

            var url = URLS.HOST + URLS.Series.Search.replace(':name', $scope.seriesName);

            $http.get(url).then(
                function (response) {
                    $scope.series = response.data;
                    $scope.isLoading = false;
                    $scope.searched = true;
                },
                function (err) {
                    console.warn (err);
                }
            );
        }
    }

    $scope.checkForEnter = function (event) {
        if (event.key === "Enter") {
            $scope.search();
        }
    }

    $scope.utils = utils;

    $scope.$watch('model.season', function (newValue, oldValue) {
        if (newValue) {
            $scope.progress = 75;

            var episodes = $scope.seasons.filter(function (item) {
                return item.number === $scope.model.season;
            })[0].episodes;

            for (var i = 0; i < episodes.length; i++) {
                episodes[i].episodeName = episodes[i].episode;
            }

            episodes.unshift({
                episodeName: 'No episodes seen this season',
                episode: 0,
                id: 0
            });

            $scope.episodes = episodes;
        }
    });

    $scope.$watch('model.episode', function (newValue, oldValue) {
        if (newValue) {
            $scope.progress = 90;
        }
    });

    $scope.selectSeason = function () {
        $scope.progress = 50;
        $scope.step = 1;

        $scope.isLoading = true;

        var url = URLS.HOST + URLS.Series.Stats.replace(':id', $scope.model.series.id);
        $http.get(url).then(
            function (response) {
                $scope.seasons = response.data;
                $scope.isLoading = false;
            }
        )
    }

    $scope.finish = function () {
        $scope.progress = 100;
        $scope.step = 2;
        $scope.isLoading = true;

        var url = URLS.HOST + URLS.Series.Add;

        var data = {
            name: $scope.model.series.seriesName,
            seriesId: $scope.model.series.id,
            //TODO: remove absoluteNumber from everywhere
            episodeId: 1,
            // episodeId: $scope.model.episode.absoluteNumber,
            season: $scope.model.season,
            episode: $scope.model.episode.episode,
            limit: $scope.model.limit,
            endSeason: $scope.model.endSeason,
            endEpisode: $scope.model.endEpisode ? $scope.model.endEpisode.episode : null,
            status: utils.convertStatusToInt($scope.model.series.status),
            aliases: $scope.model.series.aliases
        };

        $http.post(url, data).then(
            function (response) {
                $scope.isLoading = false;
                toaster.showToaster('Series ' + $scope.model.series.seriesName + ' added.', ALERT_TYPE.Success);
                $scope.$close(true);
            },
            function (err) {
                console.warn(err);
            }
        );
    }
}]);