angular.module('vids.ui').controller('videosEdit', ['$scope', 'item', 'toaster', 'ALERT_TYPE', '$http', 'URLS', controller]);


function controller($scope, item, toaster, ALERT_TYPE, $http, URLS) {
    $scope.item = angular.copy(item);
    $scope.item.IsViewed = $scope.item.IsViewed.toString();
    
    $scope.save = function () {
        var url = URLS.HOST + URLS.Videos.Edit.replace(':id', $scope.item.Id);

        var obj = angular.copy($scope.item);
        obj.IsViewed =  obj.IsViewed === 'true' ? true : false;

        $http.patch(url, obj).then(
            function (response) {
                toaster.showToaster('Item ' + item.Name + ' updated.', ALERT_TYPE.Success);

                item.Name = $scope.item.Name;
                item.IsViewed = $scope.item.IsViewed === 'true' ? true : false;

                $scope.$close(true);        
            },
            function (err) {
                console.warn(err);
            }
        );
        
    }
}