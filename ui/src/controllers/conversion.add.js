angular.module('vids.ui').controller('conversionAdd', ['$scope', 'toaster', 'ALERT_TYPE', '$http', 'URLS', '$q', 
function ($scope, toaster, ALERT_TYPE, $http, URLS, $q) {

    $scope.items = [];
    $scope.isLoading = true;

    function init () {
        var url = URLS.HOST + URLS.Conversions.FileList;

        $http.get(url).then(
            function (response) {
                $scope.items = response.data;
                $scope.isLoading = false;
            },
            function (err) {
                console.warn (err);
            }
        );
    };

    $scope.finish = function () {
        var url = URLS.HOST + URLS.Conversions.Add;
        var requests = [];
        var checkboxes = $('#modal-body :checkbox');

        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                requests.push($http.post(url, { relativePath: checkboxes[i].value}));
            }
        }

        if (requests.length == 0) {
            toaster.showToaster('No files selected', ALERT_TYPE.Warning);

            return;
        }

        $q.all(requests).then(
            function (responses) {
                $scope.isLoading = false;
                toaster.showToaster('Files added.', ALERT_TYPE.Success);
                $scope.$close(true);
            },
            function (err) {
                console.warn(err);
            }
        );
    };

    init();
}]);