angular.module('vids.ui').controller('settings', ['$scope', '$http', 'URLS', 'toaster', 'ALERT_TYPE', function ($scope, $http, URLS, toaster, ALERT_TYPE) {
    $scope.isLoading = false;

    var url = URLS.HOST + URLS.Videos.Update;

    $scope.update = function () {
        $scope.isLoading = true;

        $http.post(url).then(function (response) {
            $scope.isLoading = false;

            toaster.showToaster('Videos list updated.', ALERT_TYPE.Success);
        },
        function (err) {
            console.error(err);
            toaster.showToaster('Failed to update videos list.', ALERT_TYPE.Danger);
            toaster.showToaster(err, ALERT_TYPE.Danger);
        });
    };
}]);