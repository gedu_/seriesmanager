angular.module('vids.ui').directive('downloads', [function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/directives/downloads.html',
        scope: {
            data: '=',
            refresh: '=',
            delete: '='
        }
    };
}]);