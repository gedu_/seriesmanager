angular.module('vids.ui').directive('path', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/directives/path.html',
        scope: {
            data: '=',
            goToFunc: '=',
            base: '='
        },
        controller: function($scope) {
            
            $scope.$watchCollection('data', function (newValue, oldValue) {
                var tmp = angular.copy($scope.data);
                tmp.splice(tmp.length -1, 1);

                $scope.paths = tmp;
                $scope.lastPath = angular.copy($scope.data[$scope.data.length - 1]);
            });
        }
    };
});