angular.module('vids.ui').directive('toaster', ['toaster', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/directives/toaster.html',
        controller: function($scope, toaster) {
            $scope.closeAlert = function (index) {
                toaster.hideToaster(index);
            }           
        }
    };
}]);