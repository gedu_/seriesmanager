angular.module('vids.ui').directive('conversions', ['$uibModal', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/directives/conversions.html',
        scope: {
            data: '=',
            refresh: '=',
            delete: '='
        },
        controller: function ($scope, $uibModal) {
            $scope.add = function () {
                var modal = $uibModal.open({
                    controller: 'conversionAdd',
                    size: 'lg',
                    templateUrl: 'templates/modals/conversion.add.html',
                });
        
                modal.result.then(
                    function (res) {
                        if (res) {
                            $scope.refresh();
                        }
                    },
                    function () {
                        // Workaround to avoid console errors about dismissal
                    }
                );
            };

            $scope.fixStatus = function (item) {
                var statuses = ['Not Started', 'Uploading', 'Converting', 'Downloading', 'Finished'];

                return statuses[item];
            };
        }
    };
}]);