angular.module('vids.ui').directive('logs', [function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/directives/logs.html',
        scope: {
            data: '=',
            refresh: '='
        },
        controller: ['$scope', function ($scope) {
            $scope.model = {
                day: new Date()
            }

            $scope.update = function (date) {
                if (date !== null) {
                    $scope.refresh(date.getTime());
                }
            }

            $scope.convertDate = function (date) {
                return new Date(date).toLocaleString();
            }

            $scope.convertLevel = function (level) {
                switch (level) {
                    case 0: return "error";
                    case 1: return "warn";
                    case 2: return "info";
                    case 3: return "verbose";
                    case 4: return "debug";
                    default: return "silly";
                }
            }
        }]
    };
}]);