angular.module('vids.ui').directive('series', ['utils', function (utils) {
    return {
        restrict: 'E',
        templateUrl: 'templates/directives/series.html',
        scope: {
            data: '=',
            add: '=',
            edit: '=',
            delete: '='
        },
        controller: function($scope) {    
            $scope.convertStatus = utils.getStatus;
            
            $scope.convertAliases = utils.getAliases2; 

            $scope.convertLastModified = function (lastModified) {
                var date = new Date(lastModified * 1000);
                return date.toLocaleDateString('lt-LT') + ' ' + date.toLocaleTimeString('lt-LT');
            }
        }
    };
}]);