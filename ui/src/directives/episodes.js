angular.module('vids.ui').directive('episodes', [function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/directives/episodes.html',
        scope: {
            data: '='
        },
        controller: function($scope) {    
            $scope.convertTime = function (time) {
                var date = new Date(time * 1000);
                return date.toLocaleDateString('lt-LT') + ' ' + date.toLocaleTimeString('lt-LT');
            }
        }
    };
}]);