angular.module('vids.ui').directive('videoTile', [function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/directives/video.tile.html',
        scope: {
            item: '=',
            callbacks: '='
        },
        controller: function($scope) {  
            var regexp = new RegExp(/\.S\d\dE\d\d/);

            function addExtraZero(item) {
                return item < 10 ? "0" + item : item;
            }

            $scope.hasSeason = function () {
                return regexp.test($scope.item.Name);
            }

            $scope.getSeason = function () {
                return regexp.exec($scope.item.Name)[0].substr(1); 
            }

            $scope.getItemCount = function () {
                return $scope.item.Children.filter(function (it) {
                    return $scope.callbacks.viewPredicate(it);
                }).length;
            };
        
            $scope.getLength = function () {
                var length = $scope.item.Length;
                var hours = Math.floor(length / 3600);
                var minutes = Math.floor(length / 60);
                var seconds = length % 60;
        
                // At least 1 hour
                if (hours > 0) {
                    minutes = Math.floor((length - (3600 * hours)) / 60);
        
                    return addExtraZero(hours) + ":" + addExtraZero(minutes) + ":" + addExtraZero(seconds);
                }
        
                return addExtraZero(minutes) + ":" + addExtraZero(seconds);
            };
        
            $scope.getSize = function (size) {
                var size = $scope.item.Size;
                var byteSize = 1024;
                var sizes = [
                    'Bytes', 
                    'KB', 
                    'MB', 
                    'GB', 
                    'TB', 
                    'PB', 
                    'EB', 
                    'ZB', 
                    'YB'
                ];
        
                var i = Math.floor(Math.log(size) / Math.log(byteSize));
                
                return parseFloat((size / Math.pow(byteSize, i)).toFixed(2)) + ' ' + sizes[i];
            };
        }
    };
}]);