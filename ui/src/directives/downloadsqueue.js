angular.module('vids.ui').directive('downloadsQueue', [function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/directives/downloadsqueue.html',
        scope: {
            data: '=',
            refresh: '=',
            delete: '='
        },
        controller: function ($scope) {
            // TODO: implement popover to show aliases
            $scope.handleAliases = function (input) {
                return input.split(';')[0];
            };

            $scope.convertTime = function (time) {
                var date = new Date(time * 1000);
                return date.toLocaleDateString('lt-LT') + ' ' + date.toLocaleTimeString('lt-LT');
            };
        }
    };
}]);