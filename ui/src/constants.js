angular.module('vids.ui').constant('ALERT_TYPE', {
    Primary: 'primary',
    Secondary: 'secondary',
    Success: 'success',
    Danger: 'danger',
    Warning: 'warning',
    Info: 'info',
    Light: 'light',
    Dark: 'dark'
});

angular.module('vids.ui').constant('URLS', {
    // TODO: change it from here
    HOST: 'http://192.168.1.2:3000/',
    VIDEOS_HOST: 'http://vids.repo/',
    Downloads: {
        Active: 'downloads/active',
        Past: 'downloads/past',
        Delete: 'downloads/:id',
        Queue: {
            List: 'downloads/queue',
            Delete: 'downloads/queue/:id'
        }
    },
    Conversions: {
        List: 'conversions',
        Delete: 'conversions/:id',
        Add: 'conversions',
        FileList: 'conversions/files'
    },
    Videos: {
        All: 'videos/all',
        Edit: 'videos/:id/',
        Delete: 'videos/:id/',
        Update: 'videos/update'
    },
    Series: {
        Search: 'series/by_name?name=:name',
        Stats: 'series/:id/stats',
        Add: 'series/',
        List: 'series/',
        Episodes: 'series/episodes',
        Edit: 'series/:id',
        Delete: 'series/:id'
    },
    Logs: {
        Today: 'logs/',
        AnyDay: 'logs/:timestamp'
    }
});
