angular.module('vids.ui').service('toaster', ['$timeout', '$rootScope', service]);

function service ($timeout, $rootScope) {

    function showToaster (message, type, dismiss) {
        var dismissTimer = angular.isUndefined(dismiss) ? 3000 : dismiss;
        var alertType = angular.isUndefined(type) ? 'warning' : type;

        $rootScope.alerts.push({
            type: alertType,
            message: message,
            dismiss: dismissTimer
        });
    }

    function hideToaster (index) {
        $rootScope.alerts.splice(index, 1);
    }

    return {
        showToaster: showToaster,
        hideToaster : hideToaster
    };
}