angular.module('vids.ui').service('utils', [function () {



    function getAliases (aliases) {
        if (aliases.length > 0) {
            return aliases.reduce(function (accum, val) {
                return accum.length > 0 ? accum + ', ' + val : val;
            }, '');
        }
        
        return '';
    }

    function getAliases2 (aliases) {
        return aliases ? aliases.replace(/;/g, ", ") : '';
    }

    function getStatus (status) {
        switch (status) {
            case 0:
                return 'Ended';
            case 1:
                return 'Continuing';
            default: 
                return 'Unknown';
        }
        
    }

    function convertStatusToInt (status) {
        switch (status) {
            case 'Ended':
                return 0;
            case 'Continuing':
                return 1;
            default: 
                return 2;
        }
        
    }

    return {
        getAliases: getAliases,
        getStatus: getStatus,
        getAliases2: getAliases2,
        convertStatusToInt: convertStatusToInt
    }
}]);