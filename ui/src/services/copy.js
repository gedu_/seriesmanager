angular.module('vids.ui').service('copyService', ['$window', function ($window) {

    function copy (data) {
        var body = angular.element($window.document.body);
        var textarea = angular.element('<textarea/>');
        
        textarea.css({
            position: 'fixed',
            opacity: '0'
        });

        textarea.val(data);
        body.append(textarea);
        textarea[0].select();

        try {
            var successful = document.execCommand('copy');
            
            if (!successful) {
                throw successful;
            }
        } catch (err) {
            window.prompt('Copy to clipboard: Ctrl+C, Enter', data);
        }

        textarea.remove();
    }

    return {
        copy: copy
    }
}]);
