

angular.module('vids.ui').config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {

    $stateProvider.state({
        name: 'videos',
        url: '/videos',
        controller: 'videos',
        templateUrl: 'templates/controllers/videos.html'
    });

    $stateProvider.state({
        name: 'settings',
        url: '/settings',
        templateUrl: 'templates/controllers/settings.html',
        controller: 'settings'
    });

    $stateProvider.state({
        name: 'downloads',
        url: '/downloads',
        templateUrl: 'templates/controllers/downloads.html',
        controller: 'downloads'
    });

    $stateProvider.state({
        name: 'series',
        url: '/series',
        templateUrl: 'templates/controllers/series.html',
        controller: 'series'
    });

    $stateProvider.state({
        name: 'logs',
        url: '/logs',
        templateUrl: 'templates/controllers/logs.html',
        controller: 'logs'
    });

    $urlRouterProvider.when('', '/videos');


}]);

angular.module('vids.ui').run(['$rootScope', '$transitions', function($rootScope, $transitions) {

    $rootScope.activeMenuItem = 'videos';
    $rootScope.alerts = [];

    $transitions.onSuccess({ to: '*' }, function (transition) {
        $rootScope.activeMenuItem = transition.router.globals.current.name;
    });
}]);