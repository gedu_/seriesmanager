import * as express from "express";
import * as mysql from "mysql";
import * as cors from "cors";
import * as bodyParser from "body-parser";
import * as winston from "winston";

import * as connector from "./core/dbConnector";
import { Matcher } from "./core/matcher";
import * as utils from "./core/utils";
import * as transmission from "./core/transmission";
import * as updateChecker from "./core/updateChecker";
import * as conversionChecker from "./core/conversionChecker";
import * as tvdbUtils from "./core/tvdbUtils";
import { set, execute } from "./core/fpUtils";

import { Config } from "./data/config";

import * as downloadsController from "./controllers/downloads";
import * as videosController from "./controllers/videos";
import * as seriesController from "./controllers/series";
import * as logsController from "./controllers/logs";

let _express : any = express();
let _router : any = express.Router();
let _config : Config;
let _connectionPool : mysql.Pool;
let _logger : winston.Logger;
let _configFile : string = "config.json";

function start (): void {
    new Matcher<string>({logger: null})
        .setValueAsync(() => utils.readFile(_configFile))
        .then(x => x.execute(y => set(y, _config = JSON.parse(y))))
        .then(x => x.execute(y => init()))
        .then(x => x.onError(y => initFailed(y)));
}
// todo: start server and return 503 for every route ?

function initFailed (error : string): string {
    console.error(`Failed to read config file ${_configFile}: ${error}`);

    return error;
}

function init (): void {
    let port : number = _config.Port;
    let corsUrls : string[] = _config.Cors;

    _logger = winston.createLogger({
        level: "debug",
        format: winston.format.printf((info) => `[${new Date().toISOString()}][${info.level}] ${info.message}`),
        transports: [
            new winston.transports.Console(),
            new winston.transports.File({
                filename: _config.Logfile
            })
        ]
    });

    _express.use(cors({
        origin: (origin, callback) => {
            if (corsUrls.indexOf(origin) > -1) {
                callback(null, true);
            } else {
                callback(new Error("Not allowed by CORS"));
            }
        }

    }));

    _express.use(bodyParser.json());

    _express.options("*", cors());

    _connectionPool = connector.getConnectionPool(_config.Database);


    _router.get("/downloads/active", (request, response) => downloadsController.getActive(request, response, _connectionPool, _logger));
    _router.get("/downloads/past", (request, response) => downloadsController.getPast(request, response, _connectionPool, _logger));
    _router.delete("/downloads/:id", (request, response) => downloadsController.deleteItem(request, response, _connectionPool,
        _config, _logger));

    _router.get("/downloads/queue", (request, response) => downloadsController.getQueueItems(request, response, _connectionPool, _logger));
    _router.delete("/downloads/queue/:id", (request, response) => downloadsController.deleteQueueItem(request, response, _connectionPool, _logger));
    _router.get("/conversions", (request, response) => downloadsController.getConversions(request, response, _connectionPool, _logger));
    _router.delete("/conversions/:id", (request, response) => downloadsController.deleteConversion(request, response, _connectionPool, _logger));
    _router.post("/conversions", (request, response) => downloadsController.addConversion(request, response, _connectionPool, _config));
    _router.get("/conversions/files", (request, response) => downloadsController.getConversionsFiles(request, response, _config, _logger));

    _router.get("/videos/all", (request, response) => videosController.getAll(request, response, _connectionPool, _config, _logger));
    _router.patch("/videos/:id", (request, response) => videosController.patchItem(request, response, _connectionPool, _logger));
    _router.post("/videos/update", (request, response) => videosController.updateItems(request, response, _connectionPool,
        _config, _logger));

    _router.get("/series/by_name", (request, response) => seriesController.getByName(request, response, _connectionPool, _config, _logger));
    _router.get("/series/:id/stats", (request, response) => seriesController.getStats(request, response, _connectionPool,
        _config, _logger));
    _router.get("/series/", (request, response) => seriesController.getAll(request, response, _connectionPool, _config, _logger));
    _router.post("/series/", (request, response) => seriesController.addSeries(request, response, _connectionPool, _config, _logger));
    _router.get("/series/episodes", (request, response) => seriesController.getEpisodes(request, response, _connectionPool,
        _config, _logger));

    _router.delete("/series/:id", (request, response) => seriesController.deleteSeries(request, response, _connectionPool, _config, _logger));
    _router.patch("/series/:id", (request, response) => seriesController.updateSeries(request, response, _connectionPool, _config, _logger));

    _router.get("/logs/", (request, response) => logsController.getToday(request, response, _config));
    _router.get("/logs/:day", (request, response) => logsController.getDay(request, response, _config));

    _router.get("/", async (request, response) => {
        response.send("Swagger here or something !");
    });

    _express.use(_router);

    if (_config.Tvdb.Enabled) {
        setInterval(() => {
            tvdbUtils.getToken(_connectionPool, _config, _logger)
                .then(x => x.executeAsync(y => updateChecker.checkForSeriesUpdates(_connectionPool, y, _logger, _config)))
                .then(x => x.onError(y => execute(y, z => _logger.error(`Error in interval. ${z}`))));
        }, _config.UpdateCheckIntervalMinutes * 60 * 1000);
    }


    if (_config.Transmission.Enabled) {
        setInterval(async () => await updateChecker.checkForDownloadsUpdates(_connectionPool, _config, _logger),
            _config.UpdateCheckIntervalMinutes * 60 * 1000);
        setInterval(async () => await transmission.updateStatuses(_connectionPool, _config, _logger), 30 * 1000);
    }

    if (_config.VCS.Enabled) {
        setInterval(async () => await conversionChecker.check(_connectionPool, _config, _logger), 30 * 1000);
    }

    _express.listen(port, (err) => {
        if (err) {
            _logger.error(err);
            return;
        }

        return _logger.debug(`server is listening on ${port}`);
    });
}

start();
