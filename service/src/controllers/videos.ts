import * as mysql from "mysql";
import * as express from "express";
import * as fileTree from "../core/fileTree";
import { FileItem } from "../data/fileItem";
import * as utils from "../core/utils";
import { Matcher } from "../core/matcher";
import { Config } from "../data/config";
import * as videosUtils from "../core/videosUtils";
import * as winston from "winston";

async function getAll (request : express.Request, response : express.Response, connection : mysql.Pool, config : Config, logger : winston.Logger):
    Promise<void> {

    try {
        let files : FileItem[] = await fileTree.get(connection, config, logger);
        response.json(files);
    } catch (err) {
        utils.handleError(response, err, logger);
    }
}

function patchItem (request : express.Request, response : express.Response, connection : mysql.Pool, logger : winston.Logger): void {
    new Matcher<FileItem>({logger: logger})
        .setValueAsync(() => videosUtils.getFileItem(request))
        .then(x => x.executeAsync(y => videosUtils.updateItem(connection, y.Name, y.IsViewed ? 1 : 0, request.params.id, logger)))
        .then(x => x.execute(y => response.sendStatus(204)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)));
}

async function updateItems (request : express.Request, response : express.Response, connection : mysql.Pool, config : Config,
    logger : winston.Logger): Promise<void> {

    try {
        await fileTree.update(connection, config, logger);

        response.sendStatus(200);
    } catch (e) {

        utils.handleError(response, e, logger);
    }
}


export { getAll, patchItem, updateItems };