import * as mysql from "mysql";
import * as express from "express";
import * as tvdb from "../core/tvdb";
import * as tvdbUtils from "../core/tvdbUtils";
import * as db from "../core/db";
import { Matcher } from "../core/matcher";
import { Token } from "../data/token";
import { Config } from "../data/config";
import { SeriesDescription } from "../data/seriesDescription";
import { Series } from "../data/series";
import { SeriesEpisode } from "../data/seriesEpisode";
import { SeriesSeason } from "../data/seriesSeason";
import * as utils from "../core/utils";
import { set, setAsync } from "../core/fpUtils";
import { IObjectified } from "../interfaces/ibojectified";
import { objectify } from "../core/utils";
import * as winston from "winston";
import * as updateChecker from "../core/updateChecker";
import { Episode } from "../data/episode";

function getByName (request : express.Request, response : express.Response, connection : mysql.Pool, config : Config,
    logger : winston.Logger): void {

    let token : IObjectified<Token> = objectify({
        Id: 1,
        Token: "",
        AcquiredTime: 1,
        NewToken: false
    });

    tvdbUtils.getToken(connection, config, logger)
        .then(x => x.execute(y => set(y, token)))
        .then(x => new Matcher<SeriesDescription[]>({ error: x.getError(), logger: logger}))
        .then(x => x.continueMatcherAsync(y => tvdb.searchSeries(token.value, request.query.name, logger)))
        .then(x => x.continueAsync(y => tvdbUtils.markExistingSeries(connection, logger, y)))
        .then(x => x.execute(y => response.json(y)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)))
        .then(x => x.onStopped(y => utils.handleNoData(response, y)));
}

function getStats (request : express.Request, response : express.Response, connection : mysql.Pool, config : Config,
    logger : winston.Logger): void {

    let episodes : IObjectified<SeriesEpisode[]> = objectify([]);

    let token : IObjectified<Token> = objectify({
        Id: 1,
        Token: "",
        AcquiredTime: 1,
        NewToken: false
    });

    tvdbUtils.getToken(connection, config, logger)
        .then(x => x.execute(y => set(y, token)))
        .then(x => new Matcher<SeriesEpisode[]>({ error: x.getError(), logger: logger}))
        // todo: sanitize params.id ?
        .then(x => x.continueAsync(y => tvdb.getEpisodes(token.value, logger, request.params.id)))
        .then(x => x.execute(y => set(y, episodes)))
        .then(x => new Matcher<SeriesSeason[]>({ error: x.getError(), logger: logger }))
        .then(x => x.continueAsync(y => tvdbUtils.groupSeasons(episodes.value)))
        .then(x => x.continue(y => tvdbUtils.sortGroupedSeasons(y)))
        .then(x => x.execute(y => response.json(y)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)));
}

function getAll (request : express.Request, response : express.Response, connection : mysql.Pool, config : Config,
    logger : winston.Logger): void {

    let series : IObjectified<Series[]> = objectify([]);

    tvdbUtils.getToken(connection, config, logger)
        .then(x => x.executeAsync(y => setAsync(tvdbUtils.getSeries(connection), series)))
        .then(x => x.execute(y => response.json(series.value)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)));
}

async function addSeries (request : express.Request, response : express.Response, connection : mysql.Pool, config : Config,
    logger : winston.Logger): Promise<void> {

    tvdbUtils.getToken(connection, config, logger)
        .then(x => x.executeAsync(y => tvdbUtils.insertSeries(connection, request.body)))
        .then(x => x.execute(y => response.sendStatus(201)))
        .then(x => x.executeAsync(y => updateChecker.checkForSeriesUpdates(connection, y, logger, config)))
        .then(x => x.executeAsync(y => updateChecker.checkForDownloadsUpdates(connection, config, logger)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)));
}

async function getEpisodes (request : express.Request, response : express.Response, connection : mysql.Pool, config : Config,
    logger : winston.Logger): Promise<void> {

        let query : string = "SELECT e.*, s.Name AS Series FROM `episodes` e JOIN series s ON e.SeriesId = s.Id " +
            "WHERE e.AiredTime >= UNIX_TIMESTAMP()";
        let data : Episode[] = await db.get<Episode[]>(connection, query);

        response.json(data);
}

async function deleteSeries (request : express.Request, response : express.Response, connection : mysql.Pool, config : Config,
    logger : winston.Logger): Promise<void> {

    let dq_query : string = "DELETE FROM `download_queue` WHERE `SeriesId` = ?";
    let e_query : string = "DELETE FROM `episodes` WHERE `SeriesId` = ?";
    let s_query : string = "DELETE FROM `series` WHERE `Id` = ?";

    try {
        await db.del(connection, dq_query, [request.params.id]);
        await db.del(connection, e_query, [request.params.id]);
        await db.del(connection, s_query, [request.params.id]);
    } catch (err) {
        logger.error(err);
        logger.error("Error deleting series " + request.params.id);
    }

    response.sendStatus(204);
}

async function updateSeries (request : express.Request, response : express.Response, connection : mysql.Pool, config : Config,
    logger : winston.Logger): Promise<void> {

    tvdbUtils.getToken(connection, config, logger)
        .then(x => x.executeAsync(y => tvdbUtils.updateSeries(connection, request.params.id, request.body)))
        .then(x => x.execute(y => response.sendStatus(204)))
        .then(x => x.executeAsync(y => updateChecker.checkForSeriesUpdates(connection, y, logger, config)))
        .then(x => x.executeAsync(y => updateChecker.checkForDownloadsUpdates(connection, config, logger)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)));
}

export { getByName, getStats, getAll, addSeries, getEpisodes, deleteSeries, updateSeries };