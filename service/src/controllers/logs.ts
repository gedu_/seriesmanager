import * as express from "express";
import { Config } from "../data/config";
import * as utils from "../core/utils";
import { LogItem } from "../data/logItem";
import { EOL } from "os";
import { LogLevel } from "../enums/logLevel";

async function getToday (request : express.Request, response : express.Response, config : Config): Promise<void> {

    let data : string = await utils.readFile(config.Logfile);

    let rows : string[] = data.split(EOL);

    let currentDate : Date = new Date();

    let logItems : string[] = rows.filter((item) => {
        if (item.length < 26) {
            return false;
        }

        let itemDate : Date = new Date(item.substr(1, item.indexOf("]")-1));

        return itemDate.getFullYear() === currentDate.getFullYear() && itemDate.getMonth() === currentDate.getMonth() &&
            itemDate.getDate() === currentDate.getDate();
    });

    let ret : LogItem[] = logItems.map((item) => {
        let firstEnd : number = item.indexOf("]");
        let itemDate : Date = new Date(item.substr(1, firstEnd - 1));
        let itemLevel : LogLevel = LogLevel[item.substr(firstEnd + 2, item.indexOf("]", firstEnd + 1) - firstEnd - 2)];
        let itemMessage : string = item.substr(item.indexOf("]", firstEnd + 1) + 2);

        return {
            date: itemDate,
            level: itemLevel,
            message: itemMessage
        };
    });

    response.json(ret);
}

async function getDay (request : express.Request, response : express.Response, config : Config): Promise<void> {

    let data : string = await utils.readFile(config.Logfile);

    let rows : string[] = data.split(EOL);

    let currentDate : Date = new Date(Number(request.params.day));

    let logItems : string[] = rows.filter((item) => {
        if (item.length < 26) {
            return false;
        }

        let itemDate : Date = new Date(item.substr(1, item.indexOf("]")-1));

        return itemDate.getFullYear() === currentDate.getFullYear() && itemDate.getMonth() === currentDate.getMonth() &&
            itemDate.getDate() === currentDate.getDate();
    });

    let ret : LogItem[] = logItems.map((item) => {
        let firstEnd : number = item.indexOf("]");
        let itemDate : Date = new Date(item.substr(1, firstEnd - 1));
        let itemLevel : LogLevel = LogLevel[item.substr(firstEnd + 2, item.indexOf("]", firstEnd + 1) - firstEnd - 2)];
        let itemMessage : string = item.substr(item.indexOf("]", firstEnd + 1) + 2);

        return {
            date: itemDate,
            level: itemLevel,
            message: itemMessage
        };
    });

    response.json(ret);
}

export { getToday, getDay };