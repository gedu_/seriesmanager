import * as mysql from "mysql";
import * as express from "express";
import { DownloadStatus } from "../data/downloadStatus";
import { Matcher } from "../core/matcher";
import * as utils from "../core/utils";
import * as downloadUtils from "../core/downloadUtils";
import { Config } from "../data/config";
import * as winston from "winston";

function getActive (request : express.Request, response : express.Response, connection : mysql.Pool, logger : winston.Logger): void {
    downloadUtils.getUnfinishedDownloads(connection, logger)
        .then(x => x.execute(y => response.json(y)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)));
}

function getPast (request : express.Request, response : express.Response, connection : mysql.Pool, logger : winston.Logger): void {
    downloadUtils.getFinishedDownloads(connection, logger)
        .then(x => x.execute(y => response.json(y)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)));
}

function deleteItem (request : express.Request, response : express.Response, connection : mysql.Pool, config : Config,
    logger : winston.Logger): void {
    downloadUtils.getItem(connection, request.params.id, logger)
        .then(x => x.execute(y => downloadUtils.deleteItem(connection, config, y, logger)))
        .then(x => x.execute(y => response.sendStatus(204)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)));
}

function getQueueItems (request : express.Request, response : express.Response, connection : mysql.Pool, logger : winston.Logger) : void {
    downloadUtils.getNotStartedDownloads(connection, logger)
        .then(x => x.execute(y => response.json(y)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)));
}

function deleteQueueItem (request : express.Request, response : express.Response, connection : mysql.Pool, logger : winston.Logger) : void {
    downloadUtils.deleteQueueItem(connection, logger, request.params.id)
        .then(x => x.execute(y =>  response.sendStatus(204)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)));
}

function getConversions (request : express.Request, response : express.Response, connection : mysql.Pool, logger : winston.Logger) : void {
    downloadUtils.getConversions(connection, logger)
        .then(x => x.execute(y => response.json(y)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)));
}

async function getConversionsFiles (request : express.Request, response : express.Response, config : Config, logger : winston.Logger) : Promise<void> {
    try {
        let files = await downloadUtils.getConversionsFiles(config, logger);

        response.json(files);
    } catch (e) {
        utils.handleError(response, e, logger);
    }   
}

function deleteConversion (request : express.Request, response : express.Response, connection : mysql.Pool, logger : winston.Logger) : void {
    downloadUtils.deleteConversion(connection, logger, request.params.id)
        .then(x => x.execute(y =>  response.sendStatus(204)))
        .then(x => x.onError(y => utils.handleError(response, y, logger)));
}

async function addConversion (request : express.Request, response : express.Response, connection : mysql.Pool, config : Config) : Promise<void> {
    await downloadUtils.addConversion(connection, config, request.body.relativePath);

    response.sendStatus(201);
}

export { getActive, getPast, deleteItem, getQueueItems, deleteQueueItem, getConversions, deleteConversion, addConversion, getConversionsFiles };