interface IMatcher<T> {
    on (predicate : (x : T) => boolean, func : (x : T) => any): IMatcher<any>;
    onAsync (predicate : (x : T) => boolean, func : (x : T) => Promise<any>): Promise<IMatcher<any>>;
    continue (func : (x : T) => T): IMatcher<T>;
    continueAsync (func : (x : T) => Promise<T>): Promise<IMatcher<T>>;
    continueMatcherAsync (func : (x : T) => Promise<IMatcher<T>>): Promise<IMatcher<T>>;
    executeAsync (func : (x : T) => Promise<any>): Promise<IMatcher<T>>;
    execute (func : (x : T) => any): IMatcher<T>;
    onError (func : (x : T) => T): IMatcher<T>;
    onStopped (func : (x : T) => T): IMatcher<T>;

    setValue (func : () => T): IMatcher<T>;
    setValueAsync (func : () => Promise<T>): Promise<IMatcher<T>>;
    setError (): IMatcher<T>;
    setStop (): IMatcher<T>;

    getValue(): T;
    getError (): boolean;
    getStopped (): boolean;
}

export { IMatcher };