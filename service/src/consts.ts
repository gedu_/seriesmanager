export const DbConnectionPoolSize : number = 10;
export const SeriesNotPermitted : string = "** 403: Series Not Permitted **";
export const Transmission_AuthFlag : string = "-n";
export const Transmission_TorrentFlag : string = "-t";
export const Transmission_InfoFlag : string = "-i";
export const Transmission_AddFlag : string = "-a";
export const Transmission_DeleteFlag : string = "--remove-and-delete";
export const Transmission_FilesFlag : string = "-f";
export const Transmission_PercentageStart : string = "Percent Done: ";
export const Transmission_PercentageEnd : string = "%";
export const Transmission_SizeStart : string = "Total size: ";
export const Transmission_SizeEnd : string = " (";
export const Transmission_NoSize : string = "0 MB";

export const FFProbe_InputFlag : string = "-i";
export const FFProbe_ShowEntries : string = "-show_entries";
export const FFProbe_Duration : string = "format=duration";
export const FFProbe_LogLevelFlag : string = "-v";
export const FFProbe_Quiet : string = "quiet";
export const FFProbe_OutputFlag : string = "-of";
export const FFProbe_OutputFormat : string = "csv=p=0";

export const Tvdb_ExpiryTime : number = 60 * 60 * 24; // 24 hours
export const TenMinutes : number = 10 * 60 * 60;
export const Tvdb_Host : string = "api.thetvdb.com";

export const Urls_SearchUrl : string = "/search/series";
export const Urls_LoginUrl : string = "/login";
export const Urls_RefreshUrl : string = "/refresh_token";
export const Urls_SeriesUrl : string = "/series/:id";
export const Urls_EpisodesUrl : string = "/series/:id/episodes";
// note: TPB seems to be blocked by ISPs, using proxy
export const Urls_TorrentHost : string = "knaben.cc";
export const Urls_TorrentSearchUrl : string = "/s/?q=:query&category=0&page=0&orderby=99";
