import * as express from "express";
import { FileItem } from "../data/fileItem";
import * as mysql from "mysql";
import * as db from "../core/db";
import { Matcher } from "./matcher";
import { IMatcher } from "../interfaces/imatcher";
import * as winston from "winston";

function getFileItemFromDb(connection : mysql.Pool, id : number): Promise<FileItem> {
    return db.getOne<FileItem>(connection, "SELECT * FROM `file_tree` where `Id` = ? LIMIT 1;", [id]);
}

function getFileItem (request : express.Request): Promise<FileItem> {
    let promise : Promise<FileItem> = new Promise<FileItem>((resolve, reject) => {
        // todo: perform integrity check here
        resolve(request.body);
    });

    return promise;
}

function updateItem (connection : mysql.Pool, name : string, isViewed : number, id : number,
    logger : winston.Logger): Promise<IMatcher<number>> {

    return new Matcher<number>({logger: logger})
        .setValueAsync(() => db.update(connection, "UPDATE `file_tree` SET `Name` = ?, `IsViewed` = ? WHERE `Id` = ? LIMIT 1;",
            [name, isViewed, id])
        )
        .then(x => x.on(y => y === 0, z => { x.setError(); return z; }));
}

export { getFileItem, updateItem, getFileItemFromDb };