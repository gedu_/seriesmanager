import { DownloadStatus } from "../data/downloadStatus";
import { Matcher } from "./matcher";
import * as db from "../core/db";
import * as mysql from "mysql";
import { IMatcher } from "../interfaces/imatcher";
import { callProcess, deleteFile } from "./utils";
import * as consts from "../consts";
import { Config } from "../data/config";
import * as fileTree from "./fileTree";
import * as winston from "winston";
import { getCorrectFiles } from "./converterFileTree";
import { TransmissionFile } from "../data/transmissionFile";
import { loopAsync } from "./fpUtils";
import { join, basename } from "path";
import { ConversionQueue } from "../data/conversionQueue";
import { DownloadQueue } from "../data/downloadQueue";
import { FileItem } from "../data/fileItem";
import * as scanner from "./scanner";
import { ConversionStatus } from "../enums/conversionStatus";

function getUnfinishedDownloads (connection : mysql.Pool, logger : winston.Logger): Promise<IMatcher<DownloadStatus[]>> {
    return new Matcher<DownloadStatus[]>({logger: logger})
        .setValueAsync(() => db.get<DownloadStatus[]>(connection, "SELECT * FROM `download_status` where `percentageDone` <> 100;"));
}

function getConversions (connection : mysql.Pool, logger : winston.Logger): Promise<IMatcher<ConversionQueue[]>> {
    return new Matcher<ConversionQueue[]>({logger: logger})
        .setValueAsync(() => db.get<ConversionQueue[]>(connection, "SELECT Id, Name, ConversionId, ConversionStatus FROM `conversion_queue`;"));
}

function getNotStartedDownloads (connection : mysql.Pool, logger : winston.Logger): Promise<IMatcher<DownloadQueue[]>> {
    return new Matcher<DownloadQueue[]>({logger: logger})
        .setValueAsync(() => db.get<DownloadQueue[]>(connection, "SELECT SeriesId, EpisodeId, AiredTime, GROUP_CONCAT(dq.Name SEPARATOR ';') as Name, s.Name as SeriesName FROM `download_queue` dq " +
        "INNER JOIN `series` s ON s.id = dq.seriesId GROUP BY `SeriesId`, `EpisodeId` ORDER BY AiredTime ASC;"));
}

function getFinishedDownloads (connection : mysql.Pool, logger : winston.Logger): Promise<IMatcher<DownloadStatus[]>> {
    return new Matcher<DownloadStatus[]>({logger: logger})
        .setValueAsync(() => db.get<DownloadStatus[]>(connection, "SELECT * FROM `download_status` where `percentageDone` = 100;"));
}

function getItem (connection : mysql.Pool, id : number, logger : winston.Logger): Promise<IMatcher<DownloadStatus>> {
    return new Matcher<DownloadStatus>({logger: logger})
        .setValueAsync(() => db.getOne<DownloadStatus>(connection, "SELECT * FROM `download_status` where `id` = ?", [id]));
}

async function deleteConvertedFile (connection : mysql.Pool, config: Config, item : TransmissionFile): Promise<void> {
    let fullPath : string = join(config.DataRoot, item.RelativePath);
    let conversion : ConversionQueue = await db.getOne<ConversionQueue>(connection,
        "SELECT * FROM `conversion_queue` WHERE `InputPath` = ?", [fullPath]);

    if (conversion !== null) {
        await deleteFile(conversion.OutputPath);
        await db.del(connection, "DELETE FROM `conversion_queue` WHERE `Id` = ? LIMIT 1", [conversion.Id]);
    }
}

function deleteItem (connection : mysql.Pool, config : Config, item : DownloadStatus, logger : winston.Logger): Promise<IMatcher<void>> {
    return new Matcher<TransmissionFile[]>({logger: logger})
        .setValueAsync(() => getCorrectFiles(config, logger, item))
        .then(x => x.executeAsync(y => loopAsync(y, z => deleteConvertedFile(connection, config, z))))
        .then(x => x.executeAsync(y => callProcess(config.Transmission.Path, logger, [config.Transmission.Host,
            consts.Transmission_TorrentFlag, item.Hash, consts.Transmission_AuthFlag, config.Transmission.Login,
            consts.Transmission_DeleteFlag]))
        )
        .then(x => x.executeAsync(y => fileTree.update(connection, config, logger)))
        .then(x => x.execute(y => db.del(connection, "DELETE FROM `download_status` WHERE `Id` = ?", [item.Id])))
        .then(x => new Matcher<void>({ error: x.getError(), logger: logger }));
}

async function deleteQueueItem (connection : mysql.Pool, logger : winston.Logger, id : number): Promise<IMatcher<number>> {
    return new Matcher<number>({logger: logger})
        .setValueAsync(() => db.del(connection, "DELETE FROM `download_queue` WHERE `EpisodeId` = ?", [id]));    
}

async function deleteConversion (connection : mysql.Pool, logger : winston.Logger, id : number): Promise<IMatcher<ConversionQueue>> {
    return new Matcher<ConversionQueue>({logger: logger})
        .setValueAsync(() => db.getOne<ConversionQueue>(connection, "SELECT * FROM `conversion_queue` WHERE `Id` = ?", [id]))
        .then(x => x.onAsync(y => y.OutputPath && y.OutputPath.length > 0, z => deleteFile(z.OutputPath)))
        .then(x => x.executeAsync(y => db.del(connection, "DELETE FROM `conversion_queue` WHERE `Id` = ? LIMIT 1", [id])));
}

async function getConversionsFiles (config : Config, logger : winston.Logger) : Promise<FileItem[]> {
    var files = await scanner.scan(config.DataRoot, config.DataRoot, config.InputVideoTypes, logger);

    files.forEach(item => item.IsDir ? item.Children = scanner.parseChildren(item, files) : item.Children = []);

    return scanner.createTree(files);
}

async function addConversion (connection : mysql.Pool, config : Config, relativePath : string) : Promise<void> {
    await db.insert(connection, "INSERT INTO `conversion_queue` (name, InputPath, ConversionStatus) VALUES (?, ?, ?);",
    [
        basename(relativePath),
        join(config.DataRoot, relativePath),
        ConversionStatus.NotStarted
    ]);
}

export { getUnfinishedDownloads, getFinishedDownloads, getItem, deleteItem, getNotStartedDownloads, 
    deleteQueueItem, getConversions, deleteConversion, getConversionsFiles, addConversion };