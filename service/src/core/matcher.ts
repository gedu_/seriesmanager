import { IMatcher } from "../interfaces/imatcher";
import * as winston from "winston";

class Matcher<T> implements IMatcher<T> {
    private val : T;
    private error : boolean;
    private stopped : boolean;
    private logger : winston.Logger;

    constructor ({value, error, stopped, logger} : {value? : T, error? : boolean, stopped? : boolean, logger : winston.Logger}) {
        this.val = value;

        this.error = error !== undefined ? error : false;
        this.stopped = stopped !== undefined ? stopped : false;
        this.logger = logger;
    }

    getValue (): T {
        return this.val;
    }

    setError (): IMatcher<T> {
        this.error = true;

        return this;
    }

    setStop (): IMatcher<T> {
        this.stopped = true;

        return this;
    }

    getStopped (): boolean {
        return this.stopped;
    }

    getError (): boolean {
        return this.error;
    }

    setValueAsync(func: () => Promise<T>): Promise<IMatcher<T>> {
        let result : Promise<IMatcher<T>> = new Promise<IMatcher<T>>((resolve, reject) => {
            if (this.stopped) {
                resolve(this);

                return;
            }

            if (this.error) {
                this.logger.info("Method 'setValueAsync' is not executed because of previous error");
                resolve(this);

                return;
            }

            func()
                .then(val => {
                    if (val instanceof Matcher) {
                        this.val = val.getValue();
                        this.error = val.getError();
                    } else {
                        this.val = val;
                    }

                    resolve(this);
                })
                .catch(err => {
                    this.error = true;
                    this.logger.error(err);
                    resolve(this);
                });
        });
        return result;
    }

    setValue(func: () => T): IMatcher<T> {
        if (this.stopped) {
            return this;
        }

        if (this.error) {
            this.logger.info("Method 'setValue' is not executed because of previous error");
            return this;
        }

        try {
            this.val = func();

        } catch (err) {
            this.error = true;
            this.logger.error(err);
        }

        return this;
    }

    on(predicate: (x: T) => boolean, func: (x: T) => any): IMatcher<any> {
        if (this.stopped) {
            return this;
        }

        if (this.error) {
            this.logger.info("Method 'on' is not executed because of previous error");
            return this;
        }

        if (predicate(this.val)) {
            this.val = func(this.val);
        }

        return this;
    }

    onAsync(predicate: (x: T) => boolean, func: (x: T) => Promise<any>): Promise<IMatcher<any>> {
        let result : Promise<IMatcher<T>> = new Promise<IMatcher<T>>((resolve, reject) => {
            if (this.stopped) {
                resolve(this);

                return;
            }

            if (this.error) {
                this.logger.info("Method 'onAsync' is not executed because of previous error");
                resolve(this);

                return;
            }

            if (predicate(this.val)) {
                func(this.val)
                    .then(val => {
                        if (val instanceof Matcher) {
                            this.val = val.getValue();
                            this.error = val.getError();
                        } else {
                            this.val = val;
                        }

                        resolve(this);
                    })
                    .catch(err => {
                        this.error = true;
                        this.logger.error(err);
                        resolve(this);
                    });
            } else {
                resolve(this);
            }

        });

        return result;
    }

    continue(func: (x: T) => T): IMatcher<T> {
        if (this.stopped) {
            return this;
        }

        if (this.error) {
            this.logger.info("Method 'then' is not executed because of previous error");
            return this;
        }

        this.val = func(this.val);

        return this;
    }

    continueAsync(func: (x: T) => Promise<T>): Promise<IMatcher<T>> {
        let result : Promise<IMatcher<T>> = new Promise<IMatcher<T>>((resolve, reject) => {
            if (this.stopped) {
                resolve(this);

                return;
            }

            if (this.error) {
                this.logger.info("Method 'thenAsync' is not executed because of previous error");
                resolve(this);

                return;
            }

            func(this.val)
                .then(val => {
                    if (val instanceof Matcher) {
                        this.val = val.getValue();
                        this.error = val.getError();
                    } else {
                        this.val = val;
                    }

                    resolve(this);
                })
                .catch(err => {
                    this.error = true;
                    this.logger.error(err);
                    resolve(this);
                });
        });

        return result;
    }

    continueMatcherAsync(func: (x: T) => Promise<IMatcher<T>>): Promise<IMatcher<T>> {
        let result : Promise<IMatcher<T>> = new Promise<IMatcher<T>>((resolve, reject) => {
            if (this.stopped) {
                resolve(this);

                return;
            }

            if (this.error) {
                this.logger.info("Method 'continueMatcherAsync' is not executed because of previous error");
                resolve(this);

                return;
            }

            func(this.val)
                .then(val => {
                    this.val = val.getValue();
                    this.error = val.getError();
                    this.stopped = val.getStopped();

                    resolve(this);
                })
                .catch(err => {
                    this.error = true;
                    this.logger.error(err);
                    resolve(this);
                });
        });

        return result;
    }

    onError(func: (x: T) => T): IMatcher<T> {
        if (this.error) {
            func(this.val);
        }

        return this;
    }

    onStopped(func: (x: T) => T): IMatcher<T> {
        if (this.stopped) {
            func(this.val);
        }

        return this;
    }

    executeAsync(func: (x: T) => Promise<any>): Promise<IMatcher<T>> {
        let result : Promise<IMatcher<T>> = new Promise<IMatcher<T>>((resolve, reject) => {
            if (this.stopped) {
                resolve(this);

                return;
            }

            if (this.error) {
                this.logger.info("Method 'executeAsync' is not executed because of previous error");
                resolve(this);

                return;
            }

            func(this.val)
                .then(val => {
                    resolve(this);
                })
                .catch(err => {
                    this.error = true;
                    this.logger.error(err);
                    resolve(this);
                });
        });

        return result;
    }

    execute(func: (x: T) => any): IMatcher<T> {
        if (this.stopped) {
            return this;
        }

        if (this.error) {
            this.logger.info("Method 'execute' is not executed because of previous error");
            return this;
        }

        try {
            func(this.val);

        } catch (err) {
            this.error = true;
            this.logger.error(err);
        }

        return this;
    }
}

export { Matcher };