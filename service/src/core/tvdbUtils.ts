import { IMatcher } from "../interfaces/imatcher";
import * as mysql from "mysql";
import * as tvdb from "../core/tvdb";
import { Config } from "../data/config";
import { Token } from "../data/token";
import { Matcher } from "./matcher";
import { SeriesDescription } from "../data/seriesDescription";
import { Series } from "../data/series";
import * as db from "./db";
import { SeriesEpisode } from "../data/seriesEpisode";
import { SeriesSeason } from "../data/seriesSeason";
import * as winston from "winston";

function getToken (connection : mysql.Pool, config : Config, logger: winston.Logger): Promise<IMatcher<Token>> {
    if (!config.Tvdb.Enabled) {
        logger.error("TVDB IS NOT ENABLED IN CONFIGURATION");

        let promise : Promise<IMatcher<Token>> = new Promise<IMatcher<Token>>((resolve, reject) => {
            resolve (new Matcher<Token>({logger: logger, error: true}));
        });

        return promise;
    }

    return tvdb.getToken(connection, logger)
        .then(x => x.onAsync(y => tvdb.isTokenExpired(y), z => tvdb.login(config.Tvdb, logger)))
        .then(x => x.onAsync(y => y.NewToken, z => tvdb.updateToken(connection, z, logger)));
}

function markExistingSeries (connection : mysql.Pool, logger : winston.Logger, series : SeriesDescription[]): Promise<SeriesDescription[]> {
    let markSeries : (element : Series) => void = (element : Series): void => {
        new Matcher<SeriesDescription>({logger: logger})
            .setValue(() => series.find(x => x.id === element.TvdbId))
            .on(x => x !== undefined, y => y.exists = true);
    };

    return new Matcher<Series[]>({logger: logger})
        .setValueAsync(() => db.get<Series[]>(connection, "SELECT * FROM `series`"))
        .then(x => x.execute(y => y.forEach(element => markSeries(element))))
        .then(x => series);
}

function getSeries (connection : mysql.Pool): Promise<Series[]> {
    return db.get<Series[]>(connection, "SELECT * FROM `series`");
}

function groupSeasons (episodes : SeriesEpisode[]): Promise<SeriesSeason[]> {
    let promise : Promise<SeriesSeason[]> = new Promise<SeriesSeason[]>((resolve, reject) => {
        let seasonCount : number = 1;
        let finished : boolean = false;

        let seasons : SeriesSeason[] = [];

        while (!finished) {
            let episodesForIteratingSeason : SeriesEpisode[] = episodes.filter(x => x.season === seasonCount);

            if (episodesForIteratingSeason.length === 0) {
                finished = true;
                break;
            }

            let season : SeriesSeason = {
                number: episodesForIteratingSeason[0].season,
                episodes: episodesForIteratingSeason
            };

            seasons.push(season);
            seasonCount++;
        }

        resolve(seasons);
    });

    return promise;
}

function sortGroupedSeasons (seasons : SeriesSeason[]): SeriesSeason[] {
    for (let i : number = 0; i < seasons.length; i++) {
        seasons[i].episodes = seasons[i].episodes.sort((a, b) => {
            if (a.episode < b.episode) {
                return -1;
            } else if (a.episode > b.episode) {
                return 1;
            }

            return 0;
        });
    }

    return seasons;
}

function insertSeries (connection : mysql.Pool, body : any): Promise<number> {
    let data : Series = {
        Id: null,
        Name: body.name,
        TvdbId: body.seriesId,
        Season: body.season,
        Episode: body.episode,
        Status: body.status,
        TvdbEpisodeId: body.episodeId,
        LastModified: 0,
        Aliases: body.aliases.length > 0 ? body.aliases.join(";") : "",
        Limit: body.limit,
        EndSeason: body.endSeason,
        EndEpisode: body.endEpisode
    };

    return db.insert(connection, "INSERT INTO `series` SET ?", data);
}

function updateSeries (connection : mysql.Pool, id : number, body : any): Promise<number> {
    return db.update(connection, "UPDATE `series` SET Season = ?, Episode = ?, LastModified = 0, `Limit` = ?, EndSeason = ?, EndEpisode = ? WHERE Id = ?", 
        [body.season, body.episode, body.limit, body.endSeason, body.endEpisode, Number(id)]);
}

function sortSeriesEpisodesByEpisodeNumber (items : SeriesEpisode[]): SeriesEpisode[] {
    return items.sort((a, b) => {
        if (a.episode < b.episode) {
            return -1;
        } else if (a.episode > b.episode) {
            return 1;
        }

        return 0;
    });
}

export {
    getToken,
    markExistingSeries,
    getSeries,
    groupSeasons,
    insertSeries,
    sortGroupedSeasons,
    sortSeriesEpisodesByEpisodeNumber,
    updateSeries
};