import * as mysql from "mysql";
import * as db from "./db";
import { FileItem } from "../data/fileItem";
import * as scanner from "./scanner";
import { FileType } from "../enums/filetype";
import { Config } from "../data/config";
import { loop, loopAsync } from "./fpUtils";
import { callProcess } from "./utils";
import * as consts from "../consts";
import * as winston from "winston";
import { FFProbe } from "../data/ffprobe";
import { basename, extname, relative } from "path";
import { SubtitleDescription } from "../data/subtitleDescription";
import { FileItemDTO } from "../data/fileItemDto";
import { FileItemV2 } from "../data/fileItemV2";

let _isRunning : boolean = false;

function getDbItems (connection : mysql.Pool): Promise<FileItem[]> {
    return db.get<FileItem[]>(connection, "SELECT * FROM `file_tree`");
}

function getDbItemsWithConversions (connection : mysql.Pool): Promise<FileItemV2[]> {
    return db.get<FileItemV2[]>(connection, "SELECT ft.*, cf.id as ConversionId, cf.language as Language, cf.path as Path FROM `file_tree` ft " +
        "LEFT JOIN converted_files cf on cf.path = absolutePath WHERE cf.id IS NOT NULL;");
}

function getDbItemsWithoutConversions (connection : mysql.Pool): Promise<FileItemV2[]> {
    return db.get<FileItemV2[]>(connection, "SELECT ft.*, '' as ConversionId, 'eng' as Language, '' as Path FROM `file_tree` ft " +
        "LEFT JOIN converted_files cf on cf.path = absolutePath WHERE cf.id IS NULL;");
}

function insertToDb (connection : mysql.Pool, item : FileItem): Promise<number> {
    return db.insert(connection,
        "INSERT INTO `file_tree` (name, extension, absolutepath, relativepath, type, level, Size, Length) VALUES (?, ?, ?, ?, ?, ?, ?, ?);",
        [
            item.Name,
            item.Extension,
            item.AbsolutePath,
            item.RelativePath,
            item.Type,
            item.Level,
            item.Size,
            item.Length
        ]);
}

function deleteFromDb (connection : mysql.Pool, item : FileItem): Promise<number> {
    return db.del(connection, "DELETE FROM `file_tree` WHERE `AbsolutePath` = ? LIMIT 1", [item.AbsolutePath]);
}

function setIsDir (item : FileItem): FileItem {
    item.IsDir = item.Type === FileType.Folder;

    return item;
}

async function setLength (logger : winston.Logger, item : FileItem, ffprobe : FFProbe): Promise<FileItem> {
    if (!item.IsDir && ffprobe.Enabled) {
        let length : string = await callProcess(ffprobe.Path, logger, [consts.FFProbe_InputFlag,
            item.AbsolutePath, consts.FFProbe_ShowEntries, consts.FFProbe_Duration, consts.FFProbe_LogLevelFlag,
            consts.FFProbe_Quiet, consts.FFProbe_OutputFlag, consts.FFProbe_OutputFormat ]);

        let temp : number = Number(length.split(".")[0]);
        item.Length = Number.isNaN(temp) ? 0 : temp;
    }

    return item;
}

function convertIsViewedToBoolean (item : FileItem): FileItem {
    if (item.IsViewed instanceof Buffer) {
        item.IsViewed = item.IsViewed[0] === 1;
    }

    return item;
}

async function update (connection : mysql.Pool, config : Config, logger : winston.Logger): Promise<void> {
    if (_isRunning) {
        return Promise.resolve();
    }

    _isRunning = true;

    let dbItems = await getDbItems(connection);

    loop(dbItems, (item) => item.Children = []);

    let diskItems = await scanner.scan(config.DataRoot, config.DataRoot, config.OutputVideoTypes, logger);

    // new items
    let newItems = scanner.findDifference(dbItems, diskItems);
    await loopAsync(newItems, (item) => setLength(logger, item, config.FFProbe));
    await loopAsync(newItems, (item) => insertToDb(connection, item));

    // old items
    let oldItems = scanner.findDifference(diskItems, dbItems);
    await loopAsync(oldItems, (item) => deleteFromDb(connection, item));

    _isRunning = false;

    return Promise.resolve();
}

async function fixSubtitles (connection : mysql.Pool, items : FileItemDTO[]): Promise<FileItemDTO[]> {
    let subLangs : SubtitleDescription[] = await db.get<SubtitleDescription[]>(connection, "SELECT * FROM `subtitles`;");

    for (let i : number = 0; i < items.length; i++) {
        let parent : FileItem = items[i];
        let relativePath : string = basename(parent.RelativePath, extname(parent.RelativePath));
        let filenameWithExtension : string = basename(parent.RelativePath);

        let subtitles : FileItem[] = items.filter(x => {
            if (x.Level !== parent.Level || parent.Type === FileType.Subtitle || parent.Type === FileType.Folder ||
                x.Type !== FileType.Subtitle) {

                return false;
            }

            if (basename(x.RelativePath).indexOf(relativePath) !== 0) {
                return false;
            }

            return true;
        });

        parent.Children = parent.Children.filter(value => value.Type !== FileType.Subtitle);
        let langs : string[] = subLangs.filter(value => value.filename === filenameWithExtension).map(value => value.language);

        for (let j : number = 0; j < subtitles.length; j++) {
            let tmp : string[] = subtitles[j].RelativePath.split("_");
            let id : number = Number(tmp[tmp.length - 1].split(".")[0]);
            subtitles[j].Name = langs[id] ? langs[id] : subtitles[j].Name;
            parent.Children.push(subtitles[j]);
        }
    }

    return items.filter(value => value.Type !== FileType.Subtitle);
}

async function get (connection : mysql.Pool, config: Config, logger : winston.Logger): Promise<FileItemDTO[]> {
    let dbFiles : FileItemV2[] = await getDbItemsWithoutConversions(connection);
    let dbFiles2 : FileItemV2[] = await getDbItemsWithConversions(connection);

    let allFiles : FileItemDTO[] = [];

    dbFiles.forEach(item => { 
        let tmp = <unknown>item as FileItemDTO; 
        tmp.Languages = [
            {
                Id: '',
                Language: 'eng',
                Path: item.RelativePath
            }
        ]; 
        allFiles.push(tmp);
    });

    let tmp = dbFiles2.map(val => val.ConversionId);
    let conversionIds = [...new Set(tmp)];

    for (let i = 0; i < conversionIds.length; i++) {
        let groupedFiles = dbFiles2.filter(val => val.ConversionId === conversionIds[i]);

        let newItem : FileItemV2 = Object.assign({}, groupedFiles[0]);

        // Removing language from title
        let startIdx = newItem.Name.indexOf('_' + groupedFiles[0].Language);
        let endIdx = startIdx + groupedFiles[0].Language.length + 2;
        newItem.Name = newItem.Name.substring(0, startIdx) + newItem.Name.substring(endIdx);

        let tmp : FileItemDTO = <unknown>newItem as FileItemDTO;
        tmp.Languages = [];

        for(let j = 0; j < groupedFiles.length; j++) {
            tmp.Languages.push({
                Id: '',
                Language: groupedFiles[j].Language,
                Path: relative(config.DataRoot, groupedFiles[j].Path) 
            });
        }

        allFiles.push(tmp);
    }    

    allFiles.forEach(item => setIsDir(item));
    allFiles.forEach(item => item.IsDir ? item.Children = scanner.parseChildren(item, allFiles) : item.Children = []);
    allFiles.forEach(item => convertIsViewedToBoolean(item));
    allFiles = await fixSubtitles(connection, allFiles);


    let files = scanner.createTree(allFiles);

    return files as FileItemDTO[];
}

export { update, get };