import * as mysql from "mysql";
import * as db from "./db";
import { Config } from "../data/config";
import { Matcher } from "./matcher";
import { IMatcher } from "../interfaces/imatcher";
import { set, loop, loopAsync, execute } from "./fpUtils";
import { IObjectified } from "../interfaces/ibojectified";
import { objectify, callProcess } from "./utils";
import * as winston from "winston";
import { extname, basename, join } from "path";
import { ConversionStatus } from "../enums/conversionStatus";
import { DownloadStatus } from "../data/downloadStatus";
import * as consts from "../consts";
import { TransmissionFile } from "../data/transmissionFile";
import { EOL } from "os";
import * as fileTree from "./fileTree";

function insertToDb (connection : mysql.Pool, config : Config, item : TransmissionFile): Promise<number> {
    return db.insert(connection,
        "INSERT INTO `conversion_queue` (name, InputPath, ConversionStatus) VALUES (?, ?, ?);",
        [
            basename(item.RelativePath),
            join(config.DataRoot, item.RelativePath),
            ConversionStatus.NotStarted
        ]);
}

function convertToFile (input : string): TransmissionFile {
    let splitted : string[] = input.split(" ").filter(x => x.length > 0);

    let ret : TransmissionFile = {
        Done: splitted[1] === "100%",
        Priority: splitted[2],
        Download: splitted[3] === "Yes",
        Size: `${splitted[4]} ${splitted[5]}`,
        RelativePath: splitted.splice(6).join(" ")
    };

    return ret;
}

function getTransmissionFiles (logger : winston.Logger, input : string): Promise<TransmissionFile[]> {
    var promise : Promise<TransmissionFile[]> = new Promise<TransmissionFile[]>((resolve, reject) => {
        let files : IObjectified<TransmissionFile[]> = objectify([]);
        let lines : IObjectified<string[]> = objectify([]);

        new Matcher<string>({ logger: logger, value: input })
            .execute(x => set(x.split(EOL), lines))
            .execute(x => loop(lines.value.splice(2, lines.value.length-3), y => files.value.push(convertToFile(y))))
            .execute(x => resolve(files.value))
            .onError(x => execute(x, y => reject(y)));
    });

    return promise;
}

function filterVideoFile (item : TransmissionFile, fileTypes : string[]): boolean {
    return fileTypes.some(x => ("." + x) === extname(item.RelativePath));
}

function getCorrectFiles (config : Config, logger : winston.Logger, item : DownloadStatus): Promise<TransmissionFile[]> {
    var transmissionInput : IObjectified<string> = objectify("");

    return new Matcher<string>({logger: logger})
        .setValueAsync(() => callProcess(config.Transmission.Path, logger, [config.Transmission.Host,
            consts.Transmission_TorrentFlag, item.Hash, consts.Transmission_AuthFlag, config.Transmission.Login,
            consts.Transmission_FilesFlag])
        )
        .then(x => x.execute(y => set(y, transmissionInput)))
        .then(x => new Matcher<TransmissionFile[]>({logger: logger, error: x.getError(), stopped: x.getStopped()}))
        .then(x => x.setValueAsync(() => getTransmissionFiles(logger, transmissionInput.value)))
        .then(x => x.continue(y => y.filter(z => filterVideoFile(z, config.InputVideoTypes))))
        .then(x => x.getValue());
}

async function updateTorrent (connection : mysql.Pool, config : Config, logger : winston.Logger, item : DownloadStatus): Promise<void> {
    if (!config.VCS.Enabled) {
        return;
    }

    let files : TransmissionFile[] = await getCorrectFiles(config, logger, item);
    logger.debug(`[ConverterFileTree] Found (${files.length}) for <${item.Title}>`);

    for (let i : number = 0; i < files.length; i++) {
        logger.debug(`[ConverterFileTree] Inserting to database <${files[i].RelativePath}>`);
        await insertToDb(connection, config, files[i]);
    }

    // if files are already in mp4 container, update file tree
    if (files.length === 0) {
        logger.debug(`[ConverterFileTree] Updating file tree`);
        await fileTree.update(connection, config, logger);
    }
}

export { updateTorrent, getCorrectFiles };
