import * as mysql from "mysql";
import * as db from "./db";
import { Series } from "../data/series";
import { Download } from "../data/download";
import * as tvdb from "./tvdb";
import { Token } from "../data/token";
import * as utils from "./utils";
import * as consts from "../consts";
import * as cheerio from "cheerio";
import { DownloadDescription } from "../data/downloadDescription";
import * as transmission from "./transmission";
import { Config } from "../data/config";
import { SeriesEpisode } from "../data/seriesEpisode";
import { sortSeriesEpisodesByEpisodeNumber } from "./tvdbUtils";
import { IObjectified } from "../interfaces/ibojectified";
import { UtilsResponse } from "../data/response";
import * as winston from "winston";
import { encodeUrl } from "./utils";

async function checkForSeriesUpdates (connection : mysql.Pool, token : Token, logger : winston.Logger, config: Config): Promise<void> {
    let series : Series[] = await db.get<Series[]>(connection, "SELECT * FROM `series`;");

    series.forEach(async element => {
        await processSeries(connection, token, element, logger, config);
    });
}

async function checkForDownloadsUpdates (connection : mysql.Pool, config : Config, logger : winston.Logger): Promise<void> {
    let downloads : Download[] = await db.get<Download[]>(connection,
        "SELECT SeriesId, EpisodeId, GROUP_CONCAT(Name SEPARATOR ';') as Name FROM `download_queue` " +
        "WHERE AiredTime <= UNIX_TIMESTAMP() GROUP BY `SeriesId`, `EpisodeId`;");

    let i : number = 0;

    downloads.forEach(async element => {
        setTimeout(async () => await processDownload(connection, config, element, logger), i * 1000);
    });
}

function convertSeriesEpisodesToDownloads (series : Series, items : SeriesEpisode[]): Download[] {
    let result : Download[] = [];

    items.forEach(element => {
        let episode : string = element.episode < 10 ? `0${element.episode}` : String(element.episode);
        let season : string = element.season < 10 ? `0${element.season}` : String(element.season);
        let nameTemplate : string = `{name}.S${season}E${episode}`;

        let name : string = nameTemplate.replace("{name}", series.Name.replace(/\s/g, "."));

        let addedTime : number = utils.getTimestamp();
        let airedTime : number = utils.convertToTimestamp(new Date(element.firstAired));

        let data : Download = {
            Name: name,
            AddedTime: addedTime,
            SeriesId: series.Id,
            EpisodeId: element.id,
            AiredTime: airedTime,
            IsAlias: false,
            Season: element.season,
            Episode: element.episode
        };

        result.push(data);

        if (series.Aliases !== null && series.Aliases !== "") {
            let allAliases : string[] = series.Aliases.split(";");

            allAliases.forEach(element => {
                let alias : string = nameTemplate.replace("{name}", element.replace(/\s/g, "."));
                let newData : Download = Object.assign({}, data);
                newData.Name = alias;
                newData.IsAlias = true;

                result.push(newData);
            });
        }
    });

    return result;
}

function insertToDownloadQueue (connection : mysql.Pool, query : string, data : Download, config: Config): Promise<number> {
    if (config.Transmission.Enabled) {
        return db.insert(connection, query, [data.Name, data.AddedTime, data.SeriesId, data.EpisodeId, data.AiredTime]);
    } else {
        return Promise.resolve(0);
    }
}

function updateNewestEpisode (connection : mysql.Pool, episodes : SeriesEpisode[], series : Series,
    lastModified : number): Promise<number> {

    let lastEpisode : SeriesEpisode = episodes[episodes.length - 1];
    let query : string = "UPDATE `series` SET `Season` = ?, `Episode` = ?, `TvdbEpisodeId` = ?, `LastModified` = ? WHERE `Id` = ? LIMIT 1";
    let data : any = [lastEpisode.season, lastEpisode.episode, lastEpisode.id, lastModified, series.Id];

    return db.update(connection, query, data);
}

function updateLastModified (connection : mysql.Pool, series : Series, lastModified : number): Promise<number> {
    let query : string = "UPDATE `series` SET `LastModified` = ? WHERE `Id` = ? LIMIT 1";
    let data : any = [lastModified, series.Id];

    return db.update(connection, query, data);
}

async function addEpisodeToDb(connection : mysql.Pool, episode : SeriesEpisode, seriesId : number): Promise<void> {
    let query : string = "INSERT INTO `episodes` (Name, SeriesId, Season, Episode, AiredTime) VALUES (?, ?, ?, ?, ?)";

    await db.insert(connection, query, [episode.name, seriesId, episode.season, episode.episode,
        utils.convertToTimestamp(episode.firstAired)]);
}

async function processSeries(connection : mysql.Pool, token : Token, series : Series, logger : winston.Logger,
    config : Config): Promise<void> {

    let query : string = "INSERT INTO `download_queue` (Name, AddedTime, SeriesId, EpisodeId, AiredTime) VALUES (?, ?, ?, ?, ?)";
    let predicate : (val : SeriesEpisode) => boolean;
    let limit : boolean = (series.Limit instanceof Buffer) ? series.Limit[0] === 1 : false;

    if (limit) {
        predicate = (val) => {
            if (val.season < series.Season || val.season > series.EndSeason) {
                return false;
            }

            if (val.season < series.EndSeason) {
                return true;
            }

            return val.episode > series.Episode && val.episode <= series.EndEpisode;
        };
    } else {
        predicate = (val) => {
            if (val.season < series.Season) {
                return false;
            }

            if (val.season > series.Season) {
                return true;
            }

            return val.episode > series.Episode;
        };
    }

    try {
        let lastModified : number = (await tvdb.seriesLastModified(logger, token, series.TvdbId)).getValue();

        if (lastModified === series.LastModified) {
            return;
        }

        let episodes : SeriesEpisode[] = await tvdb.getEpisodes(token, logger, series.TvdbId);

        if (episodes.length === 0) {
            return;
        }

        episodes = sortSeriesEpisodesByEpisodeNumber(episodes);
        episodes = episodes.filter(val => predicate(val));

        for (let i : number = 0; i < episodes.length; i++) {
            logger.debug(`[UpdateChecker] Adding episode for <${series.Name}> -> Season <${episodes[i].season}> ` +
                `Episode <${episodes[i].episode}>`);
            await addEpisodeToDb(connection, episodes[i], series.Id);
        }

        if (config.Transmission.Enabled) {
            let downloads : Download[] = convertSeriesEpisodesToDownloads(series, episodes);

            for (let i : number = 0; i < downloads.length; i++) {
                logger.debug(`[UpdateChecker] Adding episode to download list for <${series.Name}> -> Season <${downloads[i].Season}> ` +
                `Episode <${downloads[i].Episode}>`);
                await insertToDownloadQueue(connection, query, downloads[i], config);
            }
        }

        if (episodes.length > 0) {
            logger.debug(`[UpdateChecker] Updating newest episode for <${series.Name}> -> ` +
                `Season <${episodes[episodes.length - 1].season}> Episode <${episodes[episodes.length - 1].episode}>`);
            await updateNewestEpisode(connection, episodes, series, lastModified);
        } else {
            logger.debug(`[UpdateChecker] Updating last modified date to <${lastModified}> for <${series.Name}>`);
            await updateLastModified(connection, series, lastModified);
        }
    } catch (err) {
        logger.error(`[UpdateChecker] Failed to proccess series <${series.Name}>`);
        logger.error(err);
    }
}

function getDownloadDescription ($ : CheerioStatic, element : any, descriptions : IObjectified<DownloadDescription[]>,
    seriesName : string, logger: winston.Logger): void {

    let baseSelector : Cheerio = $(element.children).children(".detName");
    let title : string = baseSelector.children(".detLink").text();
    let seriesNameWithWhitespace : string = seriesName.replace(/\./g, " ").toLowerCase();
    let seriesNameWithDots : string = seriesName.toLowerCase();

    if (title.toLowerCase().indexOf(seriesNameWithDots) !== 0 && title.toLowerCase().indexOf(seriesNameWithWhitespace) !== 0) {
        logger.warn(`[UpdateChecker] Torrent name doesnt match. Series name <${seriesName}>. Torrent name <${title}>`);
        return;
    }

    let magnetLink : string = baseSelector.parent().children("a").first().attr("href");
    let metadata : string = baseSelector.parent().children(".detDesc").first().text();

    let sizeStart : number = metadata.indexOf("Size");
    let sizeEnd : number = metadata.indexOf(",", sizeStart);
    let size : string = metadata.substr(sizeStart + 5, (sizeEnd - sizeStart - 5));

    let seedersSelector : Cheerio = baseSelector.parent().next();
    let seeders : number = Number(seedersSelector.text());

    let leechersSelector : Cheerio = seedersSelector.next();
    let leechers : number = Number(leechersSelector.text());

    let hash : string = magnetLink.substr(magnetLink.indexOf("btih") + 5, 40);

    let description : DownloadDescription = {
        Title: title,
        MagnetLink: magnetLink,
        Size: size,
        Seeders: seeders,
        Leechers: leechers,
        Hash: hash
    };

    descriptions.value.push(description);
}

async function getData (path: string, host: string, logger : winston.Logger, seriesName : string, retryTime : number = 1):
    Promise<DownloadDescription[]> {

    const maxRetries : number = 3;

    if (retryTime > maxRetries) {
        return [];
    }

    let options : any = {
        host: host,
        path: consts.Urls_TorrentSearchUrl.replace(":query",encodeURIComponent(encodeUrl(path))),
        family: 4,
        method: "GET",
    };

    let $ : CheerioStatic = null;
    let descriptions : IObjectified<DownloadDescription[]> = utils.objectify([]);

    let response : UtilsResponse = await utils.requestHTTPS(options);

    if (response.meta.statusCode === 302 || response.meta.statusCode === 301) {
        let regexp : RegExp = new RegExp("(.+:\/\/)?([^\/]+)(\/.*)*");
        let tmpHosts : string[] = regexp.exec(response.meta.headers.location);

        if (tmpHosts.length > 0) {
            return await getData(path, tmpHosts[2], logger, seriesName, retryTime);
        }

        logger.warn(`[UpdateChecker] Invalid Location header <${response.meta.headers.location}>`);

        return [];
    } else if (response.meta.statusCode !== 200) {
        logger.error(`[UpdateChecker] Failed to get response from TPB (${response.meta.statusCode})`);

        return await getData(path, consts.Urls_TorrentHost, logger, seriesName, ++retryTime);
    }

    if (response.body.indexOf("Failed to contact server") > -1) {
        return await getData(path, consts.Urls_TorrentHost, logger, seriesName, ++retryTime);
    }

    $ = cheerio.load(response.body);
    let data : any = $("#searchResult");

    if (data.length === 0) {
        if (retryTime <= maxRetries) {
            return await getData(path, consts.Urls_TorrentHost, logger, seriesName, ++retryTime);
        }

        return [];
    }

    data = $("#searchResult > tbody").children();
    data.each((i, element) => getDownloadDescription($, element, descriptions, seriesName, logger));

    return descriptions.value;
}

async function startDownload (connection : mysql.Pool, download : Download, item : string,
    config : Config, logger : winston.Logger): Promise<boolean> {

    let name : string[] = item.split(".");
    name.pop();
    let seriesName : string = encodeUrl(name.join("."));

    try {
        let data : DownloadDescription[] = await getData(item, consts.Urls_TorrentHost, logger, seriesName);

        if (data.length === 0) {
            return false;
        }

        data = data.filter(value => !config.ExcludeTags.some(tag => value.Title.toLowerCase().indexOf(tag) > -1))
                   .filter(value => value.Seeders >= 5);
        sortDownloadsBySeeders(data);

        if (data.length === 0) {
            return false;
        }

        let startDownload : boolean = await transmission.startDownload(connection, config, data[0], download, logger);

        if (!startDownload) {
            logger.warn(`[UpdateChecker] Failed to start download <${seriesName} ${item}>`);
            return false;
        }

        logger.info(`[UpdateChecker] Starting download <${item}>`);

        return true;
    } catch (err) {
        logger.error("Failed to download series.");
        logger.error(err);

        return false;
    }
}

function sortDownloadsBySeeders (items : DownloadDescription[]): DownloadDescription[] {
    return items.sort((a, b) => {
        if (a.Seeders > b.Seeders) {
            return -1;
        } else if (a.Seeders < b.Seeders) {
            return 1;
        }

        return 0;
    });
}

async function processDownload (connection : mysql.Pool, config : Config, download : Download, logger : winston.Logger): Promise<void> {
    let found : boolean = false;

    let aliases : string[] = download.Name.split(";");

    for (let i : number = 0; i < aliases.length; i++) {
        if (!found) {
            found = await startDownload(connection, download, aliases[i], config, logger);
        }
    }
}

export { checkForSeriesUpdates, checkForDownloadsUpdates };
