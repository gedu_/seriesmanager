import * as mysql from "mysql";
import * as db from "./db";
import * as utils from "./utils";
import { Config } from "../data/config";
import { UtilsResponse } from "../data/response";
import * as winston from "winston";
import { ConversionQueue } from "../data/conversionQueue";
import { ConversionStatus } from "../enums/conversionStatus";
import { callProcess } from "./utils";
import { Conversion } from "../data/conversion";
import { ConversionStatusVCS } from "../enums/conversionStatusVCS";
import { dirname, join, basename } from "path";
import * as fileTree from "./fileTree";

async function queueConversion (config : Config, logger : winston.Logger, item : ConversionQueue): Promise<string> {
    let options : any = {
        host: config.VCS.Endpoint,
        path: "/convert",
        family: 4,
        method: "POST",
        port: config.VCS.Port,
        headers: {
            "content-type": "application/json"
        }
    };

    let data : any = {
        InputFileId: item.InputFileId,
        AudioCodec: "AAC",
        VideoCodec: "H264",
        Filename: basename(item.Name)
    };

    let result : UtilsResponse = await utils.requestHTTP(options, data);

    return result.body;
}

async function getConversionStatus (config : Config, logger : winston.Logger, item : ConversionQueue): Promise<Conversion> {
    let options : any = {
        host: config.VCS.Endpoint,
        path: `/convert/${item.ConversionId}`,
        family: 4,
        port: config.VCS.Port,
        method: "GET"
    };

    let result : UtilsResponse = await utils.requestHTTP(options);

    if (result.meta.statusCode === 200) {
        return JSON.parse(result.body);
    }   

    return {
        id: '',
        inputFileId: '',
        outputFiles: [],
        status: 'Failed',
        videoCodec: '',
        audioCodec: '',
        statusCode: ConversionStatusVCS.Failed,
        subtitles: []
    };
    
}

async function checkService (config : Config, logger : winston.Logger): Promise<boolean> {
    let options : any = {
        host: config.VCS.Endpoint,
        path: "/status",
        family: 4,
        port: config.VCS.Port,
        method: "GET"
    };

    return await utils.requestStatusHTTP(options);
}

async function startConversion (connection : mysql.Pool, config : Config, logger : winston.Logger,
    item : ConversionQueue): Promise<void> {
    let query : string = "UPDATE `conversion_queue` SET `ConversionStatus` = ? WHERE `Id` = ? ";
    let query2 : string = "UPDATE `conversion_queue` SET `InputFileId` = ? WHERE `Id` = ? ";
    let query3 : string = "UPDATE `conversion_queue` SET `ConversionId` = ?, `ConversionStatus` = ? WHERE `Id` = ? ";

    try {
        await db.update(connection, query, [ConversionStatus.Uploading, item.Id]);

        let fileId : string = "";

        if (config.Curl.Enabled) {
            fileId = await callProcess(config.Curl.Path, logger,
                ["-F", "file=@" + item.InputPath, `http://${config.VCS.Endpoint}:${config.VCS.Port}/file`]);
        } else {
            fileId = await utils.uploadFile(item.InputPath, `http://${config.VCS.Endpoint}:${config.VCS.Port}/file`);
        }

        fileId = fileId.replace(/"/g, "");

        await db.update(connection, query2, [fileId, item.Id]);

        item.InputFileId = fileId;

        let conversionId : string = await queueConversion(config, logger, item);

        conversionId = conversionId.replace(/"/g, "");

        await db.update(connection, query3, [conversionId, ConversionStatus.Converting, item.Id]);

    } catch (err) {
        logger.error("StartConversion error");
        logger.error(err);
        await db.update(connection, query, [item.ConversionStatus, item.Id]);
    }
}

async function checkConversion (connection : mysql.Pool, config : Config, logger : winston.Logger,
    item : ConversionQueue): Promise<void> {

    let query : string = "UPDATE `conversion_queue` SET `ConversionStatus` = ? WHERE `Id` = ?";
    let query2 : string = "UPDATE `conversion_queue` SET `ConversionStatus` = ? WHERE `Id` = ?";
    let dir : string = dirname(item.InputPath);

    try {
        let status : Conversion = await getConversionStatus(config, logger, item);

        if (status.statusCode === ConversionStatusVCS.Finished) {
            await db.update(connection, query2, [ConversionStatus.Downloading, item.Id]);

            for(let i : number = 0; i < status.subtitles.length; i++) {
                if (config.Curl.Enabled) {
                    await callProcess(config.Curl.Path, logger,
                        [`http://${config.VCS.Endpoint}:${config.VCS.Port}/file/${status.subtitles[i].id}`, "-O", "-J"], dir);
                } else {
                    await utils.downloadFile(dir, `http://${config.VCS.Endpoint}:${config.VCS.Port}/file/${status.subtitles[i].id}`);
                }

                await db.insert(connection, "INSERT INTO `subtitles` (filename, language) VALUES (?, ?);",
                    [item.Name, status.subtitles[i].language]);
            }

            for(let i : number = 0; i < status.outputFiles.length; i++) {
                if (config.Curl.Enabled) {
                    await callProcess(config.Curl.Path, logger,
                        [`http://${config.VCS.Endpoint}:${config.VCS.Port}/file/${status.outputFiles[i].id}`, "-O", "-J"], dir);
                } else {
                    await utils.downloadFile(dir, `http://${config.VCS.Endpoint}:${config.VCS.Port}/file/${status.outputFiles[i].id}`);
                }

                await db.insert(connection, "INSERT INTO `converted_files` (id, language, path) VALUES (?, ?, ?);",
                    [status.id, status.outputFiles[i].language, join(dir, status.outputFiles[i].filename)]);
            }

            await db.update(connection, query, [ConversionStatus.Finished, item.Id]);

            await fileTree.update(connection, config, logger);

            if (config.VCS.CleanupInput) {
                let options : any = {
                    host: config.VCS.Endpoint,
                    path: `/file/${status.inputFileId}`,
                    family: 4,
                    port: config.VCS.Port,
                    method: "DELETE"
                };

                let result : UtilsResponse = await utils.requestHTTP(options);

                if (result.meta.statusCode !== 204) {
                    logger.error(`Failed to delete file in VCS <${status.inputFileId}>`);
                }
            }

            if (config.VCS.CleanupOutput) {
                for(let i : number = 0; i < status.outputFiles.length; i++) {
                    let options : any = {
                        host: config.VCS.Endpoint,
                        path: `/file/${status.outputFiles[i].id}`,
                        family: 4,
                        port: config.VCS.Port,
                        method: "DELETE"
                    };

                    let result : UtilsResponse = await utils.requestHTTP(options);

                    if (result.meta.statusCode !== 204) {
                        logger.error(`Failed to delete file in VCS <${status.outputFiles[i].id}>`);
                    }
                }
            }
        }
    } catch (err) {
        logger.error("CheckConversion error");
        logger.error(err);
        await db.update(connection, query, [item.ConversionStatus, '', item.Id]);
    }
}

async function check (connection : mysql.Pool, config: Config, logger : winston.Logger): Promise<void> {
    let serviceStatus : boolean = await checkService(config, logger);

    if (serviceStatus) {
        let conversion : ConversionQueue = await db.getOne<ConversionQueue>(connection,
            "SELECT * FROM `conversion_queue` where `ConversionStatus` NOT IN (?, ?, ?)",
            [ConversionStatus.Uploading, ConversionStatus.Downloading, ConversionStatus.Finished]);

        if (conversion) {
            if (conversion.ConversionStatus === ConversionStatus.NotStarted) {
                await startConversion(connection, config, logger, conversion);
            } else if (conversion.ConversionStatus === ConversionStatus.Converting) {
                await checkConversion(connection, config, logger, conversion);
            }
        }
    }
}

export { check };
