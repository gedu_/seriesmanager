import { Token } from "../data/token";
import * as consts from "../consts";
import * as mysql from "mysql";
import * as db from "./db";
import * as https from "https";
import { Tvdb } from "../data/tvdb";
import * as utils from "./utils";
import { SeriesDescription } from "../data/seriesDescription";
import { SeriesEpisode } from "../data/seriesEpisode";
import { Matcher } from "./matcher";
import { UtilsResponse } from "../data/response";
import { execute, loop, set } from "./fpUtils";
import { IMatcher } from "../interfaces/imatcher";
import * as winston from "winston";
import { IObjectified } from "../interfaces/ibojectified";
import { encodeUrl } from "./utils";

function getTokenObject (data : any): Token {
    return {
        Id: 1,
        AcquiredTime: utils.getTimestamp(),
        Token: data.token,
        NewToken: true
    };
}

function handleUnauthorized (matcher : IMatcher<UtilsResponse>, logger : winston.Logger): IMatcher<UtilsResponse> {
    return handleError(matcher, logger, "[TvDb] Failed to login with given credentials (401)");
}

function handleError (matcher : IMatcher<any>, logger : winston.Logger, message : string): IMatcher<UtilsResponse> {
    matcher.setError();

    return matcher;
}

function handleNoData (matcher : IMatcher<UtilsResponse>): IMatcher<UtilsResponse> {
    matcher.setStop();
    return matcher;
}

function login (config : Tvdb, logger : winston.Logger): Promise<IMatcher<Token>> {
    let options : https.RequestOptions = {
        host: consts.Tvdb_Host,
        path: consts.Urls_LoginUrl,
        family: 4,
        method: "POST",
        headers: {
            "content-type": "application/json"
        }
    };

    let data : any = {
        apikey: config.Apikey,
        userkey: config.Userkey,
        username: config.User
    };

    return new Matcher<UtilsResponse>({logger: logger})
        .setValueAsync(() => utils.requestHTTPS(options, data))

        .then(x => x.on(y => y.meta.statusCode === 401, z => handleUnauthorized(x, logger)))
        .then(x => new Matcher<any>({ error: x.getError(), value: x.getValue(), logger: logger}))
        .then(x => x.setValue(() => JSON.parse(x.getValue().body)))
        .then(x => x.on(y => "Error" in y, z => handleError(x, logger, `[TvDb] Failed to login: ${z.Error}`)))
        .then(x => new Matcher<Token>({ value: getTokenObject(x.getValue()), error: x.getError(), logger: logger}))
        .then(x => x.execute(y => execute(y, z => logger.debug(`[TvDb] Succesfully logged in. Token acquired at <${z.AcquiredTime}>`))));
}


function updateToken (connection : mysql.Pool, token : Token, logger : winston.Logger): Promise<IMatcher<Token>> {
    return new Matcher<number>({logger: logger})
        .setValueAsync(() => db.update(connection,
            "UPDATE `tokens` SET `token` = ?, `acquiredTime` = ? WHERE `id` = ? LIMIT 1", [token.Token, token.AcquiredTime, token.Id])
        )
        .then(x => x.on(y => y === 0, z => handleError(x, logger, "Failed to update token in the database")))
        .then(x => new Matcher<Token>({ value: token, error: x.getError(), logger: logger}));
}

function getToken (connection : mysql.Pool, logger : winston.Logger): Promise<IMatcher<Token>> {
    return new Matcher<Token>({logger: logger})
        .setValueAsync(() => db.getOne<Token>(connection, "SELECT * FROM `tokens` LIMIT 1"));
}

function isTokenExpired (token : Token): boolean {
    let difference : number = (token.AcquiredTime + consts.Tvdb_ExpiryTime - utils.getTimestamp());

    if (difference <= consts.TenMinutes || difference < 0) {
        return true;
    }

    return false;
}

function searchSeries (token : Token, name : string, logger : winston.Logger): Promise<IMatcher<SeriesDescription[]>> {
    let options : any = {
        host: consts.Tvdb_Host,
        path: `${consts.Urls_SearchUrl}?name=${encodeURIComponent(encodeUrl(name))}`,
        family: 4,
        method: "GET",
        headers: {
            "content-type": "application/json",
            "authorization": `Bearer ${token.Token}`
        }
    };

    let filterOutBadSeries : (items: SeriesDescription[]) => SeriesDescription[] = (items : SeriesDescription[]) => {
        return items.filter(x => x.seriesName !== consts.SeriesNotPermitted);
    };

    return new Matcher<UtilsResponse>({logger: logger})
        .setValueAsync(() => utils.requestHTTPS(options))
        .then(x => x.on(y => y.meta.statusCode === 401, z => handleUnauthorized(x, logger)))
        .then(x => x.on(y => y.meta.statusCode === 404, z => handleNoData(x)))
        .then(x => new Matcher<any>({ error: x.getError(), stopped: x.getStopped(), value: x.getValue(), logger: logger}))
        .then(x => x.setValue(() => JSON.parse(x.getValue().body).data))
        .then(x => <IMatcher<SeriesDescription[]>>x.continue(y => filterOutBadSeries(y)));
}

function seriesLastModified (logger : winston.Logger, token : Token, id : number): Promise<IMatcher<number>> {
    let options : any = {
        host: consts.Tvdb_Host,
        path: consts.Urls_SeriesUrl.replace(":id", id.toString()),
        family: 4,
        method: "HEAD",
        headers: {
            "authorization": `Bearer ${token.Token}`
        }
    };

    return new Matcher<UtilsResponse>({logger: logger})
        .setValueAsync(() => utils.requestHTTPS(options))
        .then(x => x.on(y => y.meta.statusCode === 401, z => handleUnauthorized(x, logger)))
        .then(x => x.on(y => y.meta.statusCode === 404, z => handleError(x, logger, `[TvDb] No series with id ${id}`)))
        .then(x => new Matcher<any>({ error: x.getError(), value: x.getValue(), logger: logger }))
        .then(x => x.setValue(() => x.getValue().meta.headers["last-modified"]))
        .then(x => <IMatcher<number>>x.continue(y => utils.convertToTimestamp(new Date(y))));
}

function convertToSeriesEpisode (episode : any): SeriesEpisode {
    episode.season = episode.airedSeason;
    episode.episode = episode.airedEpisodeNumber;
    episode.name = episode.episodeName ? episode.episodeName : "<NO EPISODE NAME>";
    episode.firstAired = new Date(episode.firstAired);

    delete episode.episodeName;
    delete episode.airedEpisodeNumber;
    delete episode.airedSeason;

    return episode;
}

async function getEpisodes (token : Token, logger : winston.Logger, id : number, page: number = 0): Promise<SeriesEpisode[]> {

    let pager : string = page > 0 ? `?page=${page}` : "";

    let options : any = {
        host: consts.Tvdb_Host,
        path: consts.Urls_EpisodesUrl.replace(":id", id.toString()) + pager,
        family: 4,
        method: "GET",
        headers: {
            "authorization": `Bearer ${token.Token}`
        }
    };

    let response : UtilsResponse = await utils.requestHTTPS(options);

    if (response.meta.statusCode === 401) {
        logger.error("[TvDb] Failed to login with given credentials (401)");

        return [];
    } else if (response.meta.statusCode === 404) {
        logger.error(`[TvDb] No series with id ${id}`);

        return [];
    }

    let body : any = JSON.parse(response.body);

    // filter out specials and other things that are not part of the regular season also those which doesnt have airedTime
    let episodes : any = body.data.filter(value => value.airedSeason > 0 && value.firstAired.length > 0);

    let seriesEpisodes : SeriesEpisode[] = [];

    for (let i : number = 0; i < episodes.length; i++) {
        seriesEpisodes.push(convertToSeriesEpisode(episodes[i]));
    }

    if (body.links.next !== null) {
        let otherPages : SeriesEpisode[] = await getEpisodes(token, logger, id, body.links.next);
        seriesEpisodes = seriesEpisodes.concat(otherPages);
    }

    return seriesEpisodes;
}

export { login, getToken, isTokenExpired, searchSeries, seriesLastModified, updateToken, getEpisodes };