import { IObjectified } from "../interfaces/ibojectified";

function loop<T> (items : T[], fn : (item : T) => any): T[] {
    items.forEach(element => {
        fn(element);
    });

    return items;
}

function loopAsync<T> (items : T[], fn : (item : T) => Promise<any>): Promise<T[]> {
    let promises : Promise<any>[] = [];

    items.forEach(element => promises.push(fn(element)));

    return Promise.all(promises);
}

function set<T> (value : T, variableToSet : IObjectified<T>): T {
    variableToSet.value = value;

    return value;
}

function setAsync<T> (promise : Promise<T>, variableToSet : IObjectified<T>): Promise<T> {
    return new Promise<T>((resolve, reject) => {
        promise
            .then(x => {
                variableToSet.value = x;
                resolve(x);
            })
            .catch(x => reject(x));
    });
}

function execute<T> (item : T, fn : (item : T) => any): T {
    fn(item);

    return item;
}


export { loop, loopAsync, set, setAsync, execute };