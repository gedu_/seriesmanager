import { readFile as fs_readFile, readdir, Stats, lstat as fs_lstat, createReadStream, ReadStream,
    createWriteStream, WriteStream, unlink } from "fs";
import * as express from "express";
import { spawn, ChildProcess } from "child_process";
import { IObjectified } from "../interfaces/ibojectified";
import * as https from "https";
import * as http from "http";
import { ClientRequest } from "http";
import { UtilsResponse } from "../data/response";
import * as winston from "winston";
import { post, Request, CoreOptions, get } from "request";
import { join } from "path";

function getTimestamp (): number {
    return Math.round(Date.now() / 1000);
}

function convertToTimestamp (date : Date): number {
    return Math.round(date.getTime() / 1000);
}

function readFile (path : string): Promise<string> {
    let promise : Promise<string> = new Promise<string>((resolve, reject) => {
        fs_readFile(path, (err, data) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(data.toString());
        });
    });

    return promise;
}

function handleError<T> (response : express.Response, data : T, logger : winston.Logger): T {
    logger.error("Error");
    logger.error(data.toString());
    response.sendStatus(500);

    return data;
}

function handleNoData<T> (response : express.Response, data : T): T {
    response.send([]);

    return data;
}

function callProcess (command : string, logger : winston.Logger, args? : string[], cwd? : string): Promise<string> {
    let promise : Promise<string> = new Promise<string>((resolve, reject) => {
        let opts : object = cwd ? { cwd: cwd } : { };
        let proc : ChildProcess = spawn(command, args, opts);
        let buffer : string = "";
        let err : string = "";

        proc.stdout.on("data", data => buffer += data);
        proc.stderr.on("data", data => err += data);

        proc.on("close", async (code, signal) => {
            if (code === 0) {
                resolve(buffer);
            } else {
                reject(err);
            }
        });

        proc.on("error", err => logger.error(err));
    });

    return promise;
}

function readDir (path : string): Promise<string[]> {
    let promise : Promise<string[]> = new Promise<string[]>((resolve, reject) => {
        readdir(path, (err, files) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(files);
        });
    });

    return promise;
}

function lstat (path : string): Promise<Stats> {
    let promise : Promise<Stats> = new Promise<Stats>((resolve, reject) => {
        fs_lstat(path, (err, data) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(data);
        });
    });

    return promise;
}

function objectify<T> (value : T): IObjectified<T> {
    return { value: value };
}

function requestHTTP (options : http.RequestOptions, data? : any): Promise<UtilsResponse> {
    let promise : Promise<UtilsResponse> = new Promise<UtilsResponse>((resolve, reject) => {
        let request : ClientRequest = http.request(options, (response) => {
            let buffer : string = "";

            response.on("data", (data) => {
                buffer += data;
            });

            response.on("end", () => {
                if (response.statusCode !== 500) {
                    resolve({
                        body: buffer,
                        meta: response
                    });
                } else {
                    reject({
                        body: buffer,
                        meta: response
                    });
                }
            });
        });

        if (data !== undefined) {
            request.write(JSON.stringify(data));
        }

        request.end();
        request.on("error", err => reject(err));
    });

    return promise;
}

function requestStatusHTTP (options : http.RequestOptions): Promise<boolean> {
    let promise : Promise<boolean> = new Promise<boolean>((resolve, reject) => {
        let request : ClientRequest = http.request(options, (response) => {
            let buffer : string = "";

            response.on("data", (data) => {
                buffer += data;
            });

            response.on("end", () => {
                resolve(true);
            });
        });

        request.end();
        request.on("error", () => resolve(false));
    });

    return promise;
}

function requestHTTPS (options : https.RequestOptions, data? : any): Promise<UtilsResponse> {
    let promise : Promise<UtilsResponse> = new Promise<UtilsResponse>((resolve, reject) => {
        let request : ClientRequest = https.request(options, (response) => {
            let buffer : string = "";

            response.on("data", (data) => {
                buffer += data;
            });

            response.on("end", () => {
                if (response.statusCode !== 500) {
                    resolve({
                        body: buffer,
                        meta: response
                    });
                } else {
                    reject({
                        body: buffer,
                        meta: response
                    });
                }
            });
        });

        if (data !== undefined) {
            request.write(JSON.stringify(data));
        }

        request.end();

        request.on("error", err => reject(err));
    });

    return promise;
}

function uploadFile (filepath: string, url: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
        let buffer : ReadStream = createReadStream(filepath);

        let opts : CoreOptions = {
            formData: {
                file: buffer
            }
        };
        let req : Request = post(url, opts, (err, resp, body) => {
            if (err) {
                reject(err);
            } else {
                resolve(body);
            }
        });

        req.on("drain", () => {
            buffer.resume();
        });
    });
}

function downloadFile (filepath: string, url: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
        get(url)
            .on("error", err => {
                reject(err);
            })
            .on("response", resp => {
                let regexp1 : RegExp = /filename="(.*)";/gi;
                let regexp2 : RegExp = /filename=(.*);/gi;
                let filename : string = "";

                let regexp1Result : RegExpExecArray = regexp1.exec(resp.headers["content-disposition"]);
                let regexp2Result : RegExpExecArray = regexp2.exec(resp.headers["content-disposition"]);

                if (regexp1Result !== null) {
                    filename = regexp1Result[1];
                } else {
                    filename = regexp2Result[1];
                }

                let writeStream : WriteStream = createWriteStream(join(filepath, filename));
                resp.pipe(writeStream);

                resp.on("end", () => {
                    resolve("");
                });
            });
    });
}

function reverseSeparator (separator : string): string {
    return separator === "/" ? "\\" : "/";
}

function encodeUrl (input : string): string {
    return input.replace(/&/g, "and")
                .replace(/\?/g, "")
                .replace(/=/g, "")
                .replace(/#/g, "")
                .replace(/'/g, "")
                .replace(/\(/g, "")
                .replace(/\)/g, "")
                .replace(/\[/g, "")
                .replace(/\]/g, "")
                .replace(/:/g, "");
}

function deleteFile (path : string): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
        unlink(path, (err) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(true);
        });
    });
}

export { getTimestamp, convertToTimestamp, readFile, handleError, callProcess, readDir, lstat, objectify, requestHTTP,
    requestHTTPS, requestStatusHTTP, reverseSeparator, encodeUrl, uploadFile, downloadFile, handleNoData, deleteFile };
