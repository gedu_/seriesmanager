import { FileType } from "../enums/filetype";
import { Stats } from "fs";
import { FileItem } from "../data/fileItem";
import { relative, extname, basename, sep, join } from  "path";
import { Matcher } from "./matcher";
import { readDir, lstat, objectify } from "./utils";
import { loopAsync, set } from "./fpUtils";
import { IObjectified } from "../interfaces/ibojectified";
import { reverseSeparator } from "./utils";
import * as winston from "winston";

function filterOutHiddenFolders (path: string, items : string[]): string[] {
    return items.filter(async item => {
        let stats : Stats = await lstat(join(path, item));

        if (stats.isDirectory() && item[0] !== ".") {
            return true;
        }

        return false;
    });
}

function filterOutFilesWithoutCorrectType (fileTypes : string[], path : string, items : string[]): Promise<string[]> {
    let result : string[] = [];
    let promises : Promise<void>[] = [];

    let filterFile : (item : string, result : string[]) => Promise<void> = (item : string, result : string[]): Promise<void> => {
        let promise : Promise<void> = new Promise<void>((resolve, reject) => {
            lstat(join(path, item)).then(stats => {
                if (stats.isDirectory() || (!stats.isDirectory() && fileTypes.some(x => ("." + x) === extname(item)))) {
                    result.push(item);
                }

                resolve();
            }).catch(x => reject(x));
        });

        return promise;
    };

    let promise : Promise<string[]> = new Promise<string[]>((resolve, reject) => {

        for (let i : number = 0; i < items.length; i++) {
            promises.push(filterFile(items[i], result));
        }

        Promise.all(promises)
            .then(x => resolve(result))
            .catch(x => reject(x));
    });

    return promise;
}

function filterOutFiles (items : FileItem[]): FileItem[] {
    return items.filter(item => item.IsDir);
}

async function createFileItems (items : string[], basePath: string, path : string): Promise<FileItem[]> {
    let result : FileItem[] = [];

    for (let i : number = 0; i < items.length; i++) {
        let item : string = items[i];

        let fileItem : FileItem = {
            Id: 1,
            AbsolutePath: join(path, item),
            RelativePath: relative(basePath, join(path, item)),
            Extension: extname(item),
            Name: basename(item, extname(item)),
            // special case for subtitle since the only support used is vtt
            Type: extname(item) === ".vtt" ? FileType.Subtitle : FileType.Video,
            IsViewed: false,
            IsDir: false,
            Level: relative(basePath, join(path, item)).split(sep).length,
            Children: [],
            Size: 0,
            Length: 0
        };

        let stats : Stats = await lstat(join(path,item));

        if (stats.isDirectory()) {
            fileItem.IsDir = true;
            fileItem.Type = FileType.Folder;
            fileItem.Extension = "";
            fileItem.Name = basename(item);
        } else {
            fileItem.Size = stats.size;
        }

        result.push(fileItem);
    }

    return result;
}

function scanChildren(basePath : string, path : string, fileTypes : string[], items : FileItem[],
    logger : winston.Logger): Promise<FileItem[]> {

    let promise : Promise<FileItem[]> = new Promise<FileItem[]>((resolve, reject) => {
        scan(basePath, path, fileTypes, logger)
            .then(x => {
                for (let i = 0; i < x.length; i++) {
                    items.push(x[i]);
                }

                resolve(items);
            })
            .catch(x => reject(x));
    });

    return promise;
}

function scan (basePath: string, path : string, fileTypes : string[], logger : winston.Logger): Promise<FileItem[]> {
    let allFiles : IObjectified<string[]> = objectify([]);

    return new Matcher<string[]>({logger: logger})
        .setValueAsync(() => readDir(path))
        .then(x => x.continueAsync(y => filterOutFilesWithoutCorrectType(fileTypes, path, y)))
        .then(x => x.continue(y => filterOutHiddenFolders(path, y)))
        .then(x => x.execute(y => set(y, allFiles)))
        .then(x => new Matcher<FileItem[]>({ error: x.getError(), logger: logger }))
        .then(x => x.continueAsync(y => createFileItems(allFiles.value, basePath, path)))
        .then(x => x.executeAsync(y => loopAsync(filterOutFiles(y), z =>
            scanChildren(basePath, join(basePath, z.RelativePath),fileTypes, y, logger)))
        )
        .then(x => x.getValue());
}

function createTree (items : FileItem[]): FileItem[] {
    if (items.length === 0) {
        return [];
    }
    
    let filteredAndSortedItems : FileItem[] = items.filter(x => x.Type === FileType.Folder)
        .sort((a, b) => {
            if (a.RelativePath.length < b.RelativePath.length) {
                return 1;
            } else if (a.RelativePath.length > b.RelativePath.length) {
                return -1;
            }

            return 0;
        });

    let maxLevel : number = filteredAndSortedItems[0].Level;

    for(let i : number = maxLevel; i > 0; i--) {
        let itemsInLevel : FileItem[] = items.filter(x => x.Level === i && x.Type === FileType.Folder);

        itemsInLevel.forEach(element => {
            let parent : FileItem = items.find(x => x.Level === (i-1) && !x.Children.some(y => y.RelativePath === element.RelativePath));

            if (parent) {
                let self : FileItem = parent.Children.find(x => x.RelativePath === element.RelativePath);

                if (self) {
                    self.Children.push(element);
                }
            }
        });
    }


    return items.filter(x => x.Level < 2);
}

function findDifference (oldFiles : FileItem[], newFiles : FileItem[]): FileItem[] {
    let items : FileItem[] = [];

    newFiles.forEach(newFile => {
        let oldFile : FileItem = oldFiles.find(x => x.RelativePath === newFile.RelativePath);

        if (!oldFile) {
            items.push(newFile);
        } else if (newFile.Type === FileType.Folder) {
            newFile.Children.filter(x => x.Type !== FileType.Folder)
                .forEach(x => !oldFile.Children.some(y => y.RelativePath === x.RelativePath) && items.push(x));
        }
    });

    return items;
}

function parseChildren (parent : FileItem, items : FileItem[]): FileItem[] {
    var separator : string = sep;

    return items.filter(x => {
        if (parent.RelativePath === x.RelativePath) {
            return false;
        }

        if (parent.RelativePath !== "" && x.RelativePath.indexOf(separator) === -1) {
            if (x.RelativePath.indexOf(reverseSeparator(separator)) === -1) {
                return false;
            }

            separator = reverseSeparator(separator);
        }

        let baseLength : number = parent.RelativePath.length + separator.length;

        // check whether item is a child (if path doesnt start with the one specified, it would have ../ at the start)
        // check whether item is hidden or not
        if (x.RelativePath.indexOf(parent.RelativePath) === 0 && x.RelativePath.indexOf(separator, baseLength) === -1) {
            let substringed : string = x.RelativePath.substr(baseLength);

            return substringed[0] !== ".";
        }

        return false;
    });
}

export { scan, createTree, findDifference, parseChildren };
