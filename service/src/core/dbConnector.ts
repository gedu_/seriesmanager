import * as mysql from "mysql";
import * as consts from "../consts";
import { Database } from "../data/database";

function getConnectionPool(config : Database, poolSize : number = 10): mysql.Pool {
    if (!Number.isInteger(poolSize)) {
        poolSize = consts.DbConnectionPoolSize;
    }

    return mysql.createPool({
        connectionLimit : poolSize,
        host            : config.Host,
        user            : config.User,
        password        : config.Pass,
        database        : config.Db
    });
}

export { getConnectionPool };