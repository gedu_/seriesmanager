import * as mysql from "mysql";

function get<T> (connection : mysql.Pool, query : string, values : any[] = []): Promise<T> {
    let promise : Promise<T> = new Promise<T>((resolve, reject) => {
        if (values.length > 0) {
            // creating prepared statement
            query = mysql.format(query, values);
        }

        connection.query(query, (error, result, fields) => {
            if (error) {
                reject(error);

                return;
            }

            resolve(result);
        });
    });

    return promise;
}

function getOne<T> (connection : mysql.Pool, query : string, values : any[] = []): Promise<T> {
    let promise : Promise<T> = new Promise<T>(async (resolve, reject) => {
        try {
            let result : T = await get<T>(connection, query, values);

            if (result[0]) {
                resolve(result[0]);
            } else {
                resolve(null);
            }

        } catch (error) {
            reject(error);
        }
    });

    return promise;
}

function update (connection : mysql.Pool, query : string, values : any): Promise<number> {
    let promise : Promise<number> = new Promise<number>((resolve, reject) => {
        query = mysql.format(query, values);

        connection.query(query, (error, result, fields) => {
            if (error) {
                reject(error);

                return;
            }

            resolve(result.changedRows);
        });

    });

    return promise;
}

function insert (connection : mysql.Pool, query : string, data : any): Promise<number> {
    let promise : Promise<number> = new Promise<number>((resolve, reject) => {
        connection.query(query, data, (error, result, fields) => {
            if (error) {
                reject(error);

                return;
            }

            resolve(result.insertId);
        });

    });

    return promise;
}

function del (connection : mysql.Pool, query : string, values : any): Promise<number> {
    let promise : Promise<number> = new Promise<number>((resolve, reject) => {
        query = mysql.format(query, values);

        connection.query(query, (error, result, fields) => {
            if (error) {
                reject(error);

                return;
            }

            resolve(result.affectedRows);
        });

    });

    return promise;
}

export { get, getOne, update, insert, del };