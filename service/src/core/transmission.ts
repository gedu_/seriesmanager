import * as db from "./db";
import * as consts from "../consts";
import * as mysql from "mysql";
import { DownloadStatus } from "../data/downloadStatus";
import { Config } from "../data/config";
import { DownloadDescription } from "../data/downloadDescription";
import { Download } from "../data/download";
import * as utils from "./utils";
import { Matcher } from "./matcher";
import { IMatcher } from "../interfaces/imatcher";
import * as winston from "winston";
import * as converterFileTree from "./converterFileTree";

function insertStatus (connection : mysql.Pool, logger : winston.Logger, data : DownloadStatus): Promise<IMatcher<number>> {
    return new Matcher<number>({logger: logger})
        .setValueAsync(() => db.insert(connection, "INSERT INTO `download_status` SET ?", data));
}

function getPercentage (buffer : string): string {
    let percentageIndexStart : number = buffer.indexOf(consts.Transmission_PercentageStart) + consts.Transmission_PercentageStart.length;
    let percentageIndexEnd : number = buffer.indexOf(consts.Transmission_PercentageEnd, percentageIndexStart);
    let percentage : string = buffer.substr(percentageIndexStart, (percentageIndexEnd-percentageIndexStart));

    return percentage;
}

function getSize (buffer: string): string {
    let sizeIndexStart : number = buffer.indexOf(consts.Transmission_SizeStart) + consts.Transmission_SizeStart.length;
    let sizeIndexEnd : number = buffer.indexOf(consts.Transmission_SizeEnd, sizeIndexStart);
    let size : string = buffer.substr(sizeIndexStart, (sizeIndexEnd-sizeIndexStart));

    return size;
}

async function callTransmission (connection : mysql.Pool, config : Config, item : DownloadStatus, logger : winston.Logger):
    Promise<DownloadStatus> {

    let response : string = await utils.callProcess(config.Transmission.Path, logger, [config.Transmission.Host,
        consts.Transmission_TorrentFlag, item.Hash, consts.Transmission_AuthFlag, config.Transmission.Login,
        consts.Transmission_InfoFlag]);

    // Floor to an integer value, since mysql saves 99.x as 100
    let percentage : number = Math.floor(Number(getPercentage(response)));
    let size : string = getSize(response);

    if (isNaN(percentage)) {
        percentage = 0;
    }

    item.PercentageDone = percentage;
    await db.update(connection, "UPDATE `download_status` SET `percentageDone` = ?, `size` = ? WHERE `hash` = ? LIMIT 1;",
        [percentage, size, item.Hash]);

    return item;
}

async function updateStatuses (connection : mysql.Pool, config : Config, logger : winston.Logger): Promise<void> {
    try {
        let statuses : DownloadStatus[] = await  db.get<DownloadStatus[]>(connection,
            "SELECT * FROM `download_status` where `percentageDone` <> 100;");

        for (let i : number = 0; i < statuses.length; i++) {
            await callTransmission(connection, config, statuses[i], logger);
        }

        statuses = statuses.filter(value => value.PercentageDone === 100);

        for (let i : number = 0; i < statuses.length; i++) {
            logger.info(`[Transmission] Updating torrent <${statuses[i].Title}>`);
            await converterFileTree.updateTorrent(connection, config, logger, statuses[i]);
        }
    } catch (err) {
        logger.error("Failed to update download statuses");
        logger.error(err);
    }
}

function startDownload (connection: mysql.Pool, config : Config, description : DownloadDescription, download : Download,
    logger : winston.Logger): Promise<boolean> {

    return new Matcher<string>({logger: logger})
        .setValueAsync(() => utils.callProcess(config.Transmission.Path, logger, [config.Transmission.Host,
            consts.Transmission_AddFlag, description.MagnetLink, consts.Transmission_AuthFlag, config.Transmission.Login])
        )
        .then(x => new Matcher<boolean>({ error: x.getError(), logger: logger}))
        .then(x => x.executeAsync(y => insertStatus(connection, logger, { Id: null, Hash: description.Hash, Title: description.Title,
            PercentageDone: 0, Size: consts.Transmission_NoSize})))
        .then(x => x.executeAsync(y => db.del(connection, "DELETE FROM `download_queue` WHERE `SeriesId` = ? AND `EpisodeId` = ?",
            [download.SeriesId, download.EpisodeId])))
        .then(x => x.setValue(() => true))
        .then(x => x.onError(y => y = false))
        .then(x => x.getValue());
}

export { insertStatus, updateStatuses, startDownload };
