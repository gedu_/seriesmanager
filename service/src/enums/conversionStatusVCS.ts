enum ConversionStatusVCS {
    NotStarted = 0,
    Started = 1,
    Finished = 2,
    Failed = 3
}

export { ConversionStatusVCS };