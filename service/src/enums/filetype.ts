enum FileType {
    Unknown = 0,
    Folder = 1,
    Video = 2,
    Subtitle = 3
}

export { FileType };