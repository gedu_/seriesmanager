enum ConversionStatus {
    NotStarted = 0,
    Uploading = 1,
    Converting = 2,
    Downloading = 3,
    Finished = 4
}

export { ConversionStatus };