enum SeriesStatus {
    Ended = 0,
    Continuing = 1,
    Unknown = 2
}

export { SeriesStatus };