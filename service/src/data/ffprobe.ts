type FFProbe = {
    Path: string;
    Enabled: boolean;
};

export { FFProbe };