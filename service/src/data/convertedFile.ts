type ConvertedFile = {
    Id: string;
    Language: string;
    Path: string;
};

export { ConvertedFile };

