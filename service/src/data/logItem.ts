type LogItem = {
    date: Date;
    level: number;
    message: string;
};

export { LogItem };