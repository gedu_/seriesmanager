import { Database } from "./database";
import { Transmission } from "./transmission";
import { Tvdb } from "./tvdb";
import { FFProbe } from "./ffprobe";
import { Curl } from "./curl";
import { VCS } from "./vcs";

type Config = {
    Database: Database;
    Transmission: Transmission;
    Tvdb: Tvdb;
    FFProbe: FFProbe;
    Curl: Curl;
    VCS: VCS;
    ExcludeTags: string[];
    DataRoot: string;
    InputVideoTypes: string[];
    OutputVideoTypes: string[];
    UpdateCheckIntervalMinutes: number;
    Port: number;
    Cors: string[];
    Logfile: string;
};

export { Config };