import { FileType } from "../enums/filetype";

type FileItem = {
    Id: number;
    AbsolutePath: string;
    RelativePath: string;
    Extension: string;
    Name: string;
    Level: number;
    Type: FileType;
    IsViewed: boolean | Buffer;

    IsDir: boolean;
    Children: FileItem[];

    Size: number;
    Length: number;
};

export { FileItem };