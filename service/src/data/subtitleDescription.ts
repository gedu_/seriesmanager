type SubtitleDescription = {
    filename: string;
    language: string;
};

export { SubtitleDescription };