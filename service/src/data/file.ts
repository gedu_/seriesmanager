type File = {
    id: string;
    language: string;
    filename: string;
};

export { File };