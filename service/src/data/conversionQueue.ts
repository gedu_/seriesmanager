import { ConversionStatus } from "../enums/conversionStatus";

type ConversionQueue = {
    Id: number;
    Name: string;
    InputPath: string;
    OutputPath: string;
    InputFileId: string;
    OutputFileId: string;
    ConversionId: string;
    ConversionStatus: ConversionStatus;
};

export { ConversionQueue };