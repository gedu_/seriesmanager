type Tvdb = {
    User: string;
    Userkey: string;
    Apikey: string;
    Enabled: boolean;
};

export { Tvdb };