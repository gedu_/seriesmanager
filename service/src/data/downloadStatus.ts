type DownloadStatus = {
    Id: number;
    Title: string;
    Hash: string;
    PercentageDone: number;
    Size: string;
};

export { DownloadStatus };