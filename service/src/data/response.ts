import { IncomingMessage } from "http";

type UtilsResponse = {
    body: string,
    meta: IncomingMessage
};

export { UtilsResponse };