type Download = {
    Name: string;
    AddedTime: number;
    AiredTime: number;
    SeriesId: number;
    EpisodeId: number;
    IsAlias: boolean;
    Season: number;
    Episode: number;
};

export { Download };