type VCS = {
    Endpoint: string;
    Port: number;
    Enabled: boolean;
    CleanupInput: boolean;
    CleanupOutput: boolean;
};

export { VCS };