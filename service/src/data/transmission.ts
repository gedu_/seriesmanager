type Transmission = {
    Host: string;
    Login: string;
    Path: string;
    Enabled: boolean;
};

export { Transmission };