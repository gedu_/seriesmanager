type SeriesEpisode = {
    id: number;
    absoluteNumber: number;
    season: number;
    episode: number;
    name: string;
    firstAired: Date;
    lastUpdated: number;
};

export { SeriesEpisode };