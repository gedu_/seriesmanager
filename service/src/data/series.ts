import { SeriesStatus } from "../enums/seriesStatus";

type Series = {
    Id: number;
    Name: string;
    TvdbId: number;
    Season: number;
    Episode: number;
    Status: SeriesStatus;
    TvdbEpisodeId: number;
    LastModified: number;
    Aliases: string;
    Limit: boolean | Buffer;
    EndSeason: number;
    EndEpisode: number;
};

export { Series };