type Database = {
    Host: string;
    User: string;
    Pass: string;
    Db: string;
};

export { Database };

