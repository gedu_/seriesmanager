type DownloadQueue = {
    Name: string;
    AiredTime: number;
    SeriesId: number;
    EpisodeId: number;
    SeriesName : string;
};

export { DownloadQueue };