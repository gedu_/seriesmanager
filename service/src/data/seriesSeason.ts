import { SeriesEpisode } from "./seriesEpisode";

type SeriesSeason = {
    number: number;
    episodes: SeriesEpisode[];
};

export { SeriesSeason };