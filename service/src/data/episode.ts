type Episode = {
    Id: number;
    Name: string;
    SeriesId: number;
    Season: number;
    Episode: number;
    AiredTime: number;
    Series: string;
};

export { Episode };