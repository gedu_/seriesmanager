type DownloadDescription = {
    Title: string;
    MagnetLink: string;
    Size: string;
    Seeders: number;
    Leechers: number;
    Hash: string;
};

export { DownloadDescription };