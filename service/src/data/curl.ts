type Curl = {
    Path: string;
    Enabled: boolean;
};

export { Curl };