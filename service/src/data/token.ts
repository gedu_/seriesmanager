type Token = {
    Id: number;
    Token: string;
    AcquiredTime: number;
    NewToken : boolean;
};

export { Token };