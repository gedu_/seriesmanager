type TransmissionFile = {
    Done: boolean;
    Priority: string;
    Download: boolean;
    Size: string;
    RelativePath: string;
};

export { TransmissionFile };