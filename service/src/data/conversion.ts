import { ConversionStatusVCS } from "../enums/conversionStatusVCS";
import { File } from "./file";

type Conversion = {
    id: string;
    inputFileId: string;
    status: string;
    videoCodec: string;
    audioCodec: string;
    statusCode: ConversionStatusVCS;
    subtitles: File[];
    outputFiles: File[];
};

export { Conversion };