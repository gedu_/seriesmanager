import { FileItem } from "./fileItem";

type FileItemV2 = FileItem & {
    ConversionId: string;
    Language: string;
    Path: string;
};

export { FileItemV2 };