import { SeriesStatus } from "../enums/seriesStatus";

type SeriesDescription = {
    id: number;
    seriesName: string;
    status: SeriesStatus;
    firstAired: Date;
    aliases: string[];
    overview: string;
    exists: boolean;
};

export { SeriesDescription };