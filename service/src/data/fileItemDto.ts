import { FileItem } from "./fileItem";
import { ConvertedFile } from "./convertedFile";

type FileItemDTO = FileItem & {
    Languages : ConvertedFile[];
};

export { FileItemDTO };