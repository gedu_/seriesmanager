
CREATE TABLE `conversion_queue` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` tinytext NOT NULL,
  `InputPath` text NOT NULL,
  `OutputPath` text NULL,
  `InputFileId` char(36) NULL,
  `OutputFileId` char(36) NULL,
  `ConversionId` char(36) NULL,
  `ConversionStatus` tinyint(1) NOT NULL,
  PRIMARY KEY (`Id`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `download_queue` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `AddedTime` bigint(20) NOT NULL,
  `SeriesId` int(11) NOT NULL,
  `EpisodeId` int(11) NOT NULL,
  `AiredTime` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Download_Queue_SeriesId` (`SeriesId`),
  CONSTRAINT `FK_Download_Queue_SeriesId` FOREIGN KEY (`SeriesId`) REFERENCES `series` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `download_status` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Hash` varchar(40) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `PercentageDone` int(3) NOT NULL,
  `Size` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `file_tree` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` tinytext NOT NULL,
  `Extension` tinytext NOT NULL,
  `AbsolutePath` text NOT NULL,
  `RelativePath` text NOT NULL,
  `Type` tinyint(4) NOT NULL,
  `Level` tinyint(4) NOT NULL,
  `IsViewed` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `series` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `TvdbId` int(11) NOT NULL,
  `Season` int(11) NOT NULL,
  `Episode` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `TvdbEpisodeId` int(11) NOT NULL,
  `LastModified` bigint(20) NOT NULL,
  `Aliases` varchar(255) DEFAULT NULL,
  `Limit` bit(1) NOT NULL DEFAULT b'0',
  `EndSeason` int(11) NULL,
  `EndEpisode` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `episodes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `SeriesId` int(11) NOT NULL,
  `Season` int(11) NOT NULL,
  `Episode` int(11) NOT NULL,
  `AiredTime` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Episodes_SeriesId` (`SeriesId`),
  CONSTRAINT `FK_Episodes_SeriesId` FOREIGN KEY (`SeriesId`) REFERENCES `series` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `tokens` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Token` text NOT NULL,
  `AcquiredTime` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `tokens` (Token, AcquiredTime) VALUES ('123', 0);

CREATE TABLE `subtitles` (
  `Filename` text NOT NULL,
  `Language` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `converted_files` (
  `Id` CHAR(36) NOT NULL,
  `Language` char(20) NOT NULL,
  `Path` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;